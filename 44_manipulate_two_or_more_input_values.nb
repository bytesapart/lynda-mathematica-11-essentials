(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      3747,        102]
NotebookOptionsPosition[      3326,         86]
NotebookOutlinePosition[      3667,        101]
CellTagsIndexPosition[      3624,         98]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"Manipulate", "[", 
  RowBox[{
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"x", "^", "3"}], " ", "-", " ", 
      RowBox[{"a", " ", 
       RowBox[{"x", "^", "2"}]}], " ", "+", " ", "b"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"x", ",", " ", 
       RowBox[{"-", "6"}], ",", " ", "6"}], "}"}]}], "]"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"a", ",", " ", 
     RowBox[{"-", "15"}], ",", " ", "25"}], "}"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"b", ",", " ", "100", ",", " ", "1000"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.756959200930038*^9, 3.7569592713521004`*^9}},
 CellLabel->
  "In[102]:=",ExpressionUUID->"744e2687-d5e5-43d1-9d12-b3815fce2b75"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`a$$ = -0.9040107727050781, $CellContext`b$$ \
= 294.2207717895508, Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`a$$], -15, 25}, {
      Hold[$CellContext`b$$], 100, 1000}}, Typeset`size$$ = {
    360., {111., 117.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True, $CellContext`a$77208$$ = 
    0, $CellContext`b$77209$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, 
      "Variables" :> {$CellContext`a$$ = -15, $CellContext`b$$ = 100}, 
      "ControllerVariables" :> {
        Hold[$CellContext`a$$, $CellContext`a$77208$$, 0], 
        Hold[$CellContext`b$$, $CellContext`b$77209$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> 
      Plot[$CellContext`x^3 - $CellContext`a$$ $CellContext`x^2 + \
$CellContext`b$$, {$CellContext`x, -6, 6}], 
      "Specifications" :> {{$CellContext`a$$, -15, 25}, {$CellContext`b$$, 
         100, 1000}}, "Options" :> {}, "DefaultOptions" :> {}],
     ImageSizeCache->{411., {198., 204.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{{3.756959272068081*^9, 3.756959283999092*^9}},
 CellLabel->
  "Out[102]=",ExpressionUUID->"10afd489-902b-4230-b76b-40b8ad9a37b7"]
}, Open  ]]
},
WindowSize->{636, 773},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 719, 19, 48, "Input",ExpressionUUID->"744e2687-d5e5-43d1-9d12-b3815fce2b75"],
Cell[1302, 43, 2008, 40, 454, "Output",ExpressionUUID->"10afd489-902b-4230-b76b-40b8ad9a37b7"]
}, Open  ]]
}
]
*)

