(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      1360,         48]
NotebookOptionsPosition[       944,         32]
NotebookOutlinePosition[      1285,         47]
CellTagsIndexPosition[      1242,         44]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RadicalBox["64", "3"]], "Input",
 CellChangeTimes->{{3.7568844351150837`*^9, 3.756884441096094*^9}},
 CellLabel->"In[40]:=",ExpressionUUID->"2b41771b-b570-41da-aa32-fac7d31f57b0"],

Cell[BoxData["4"], "Output",
 CellChangeTimes->{3.7568844445260725`*^9},
 CellLabel->"Out[40]=",ExpressionUUID->"922b437a-b31c-4e66-9f35-b2ab2846c64a"]
}, Open  ]]
},
WindowSize->{965, 988},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 194, 3, 36, "Input",ExpressionUUID->"2b41771b-b570-41da-aa32-fac7d31f57b0"],
Cell[777, 27, 151, 2, 32, "Output",ExpressionUUID->"922b437a-b31c-4e66-9f35-b2ab2846c64a"]
}, Open  ]]
}
]
*)

