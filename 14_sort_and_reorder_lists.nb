(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      6535,        187]
NotebookOptionsPosition[      4806,        147]
NotebookOutlinePosition[      5147,        162]
CellTagsIndexPosition[      5104,        159]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"hlist", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{
   "1", ",", " ", "8", " ", ",", " ", "7", ",", " ", "1", ",", " ", "2", ",", 
    "6", ",", " ", "3", ",", " ", "5", ",", " ", "4"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.7568856156460457`*^9, 3.7568856395110416`*^9}},
 CellLabel->"In[79]:=",ExpressionUUID->"60b9642a-b8a1-4b1d-b65d-e97c3231c7cb"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "1", ",", "8", ",", "7", ",", "1", ",", "2", ",", "6", ",", "3", ",", "5", 
   ",", "4"}], "}"}]], "Output",
 CellChangeTimes->{3.756885640440034*^9},
 CellLabel->"Out[79]=",ExpressionUUID->"12c8db34-d67a-49b9-aefb-e50d105fd5d2"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"hlist", " ", "=", " ", 
  RowBox[{"Sort", "[", 
   RowBox[{"hlist", ",", " ", "Greater"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7568856433360357`*^9, 3.7568856621110835`*^9}},
 CellLabel->"In[80]:=",ExpressionUUID->"b977018c-7789-44ba-a9ec-2aa47d196ccc"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "8", ",", "7", ",", "6", ",", "5", ",", "4", ",", "3", ",", "2", ",", "1", 
   ",", "1"}], "}"}]], "Output",
 CellChangeTimes->{3.756885662793084*^9},
 CellLabel->"Out[80]=",ExpressionUUID->"771b4df9-a6b3-414d-a24e-c7e0c53a3d9d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"hlist", " ", "=", " ", 
  RowBox[{"Sort", "[", 
   RowBox[{"hlist", ",", " ", "Less"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7568856654040947`*^9, 3.7568856728160977`*^9}},
 CellLabel->"In[81]:=",ExpressionUUID->"053fc3c5-f9b9-4b89-85cb-e25408d3eeec"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "1", ",", "1", ",", "2", ",", "3", ",", "4", ",", "5", ",", "6", ",", "7", 
   ",", "8"}], "}"}]], "Output",
 CellChangeTimes->{3.756885673132082*^9},
 CellLabel->"Out[81]=",ExpressionUUID->"d977ead8-90c8-4f00-b693-47e62b6b62cb"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"jlist", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{
   "1", ",", " ", "9", ",", " ", "14", ",", " ", "3", ",", " ", "18", ",", 
    " ", "6"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.7568856786240997`*^9, 3.7568856912980876`*^9}},
 CellLabel->"In[82]:=",ExpressionUUID->"ff229102-fec9-4de1-9f85-8a3233c2b890"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"1", ",", "9", ",", "14", ",", "3", ",", "18", ",", "6"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.7568856947250814`*^9},
 CellLabel->"Out[82]=",ExpressionUUID->"4132e7c6-18d8-4831-8a69-91fcbdf6214e"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"jlist", " ", "=", " ", 
  RowBox[{"Reverse", "[", "jlist", "]"}]}]], "Input",
 CellChangeTimes->{{3.7568856974500833`*^9, 3.7568857028731065`*^9}},
 CellLabel->"In[83]:=",ExpressionUUID->"084850ff-d18b-48e2-b69f-eb13265712d5"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"6", ",", "18", ",", "3", ",", "14", ",", "9", ",", "1"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.756885703285091*^9},
 CellLabel->"Out[83]=",ExpressionUUID->"ae370c28-7af8-4516-bbfa-826f9c29b7c4"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"pad1", " ", "=", " ", 
  RowBox[{"PadLeft", "[", 
   RowBox[{"jlist", ",", " ", "9", ",", " ", "0"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7568857188510914`*^9, 3.7568857330320864`*^9}},
 CellLabel->"In[84]:=",ExpressionUUID->"0bc36551-9084-401c-a06d-2ab64bb7a9b0"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "0", ",", "0", ",", "0", ",", "6", ",", "18", ",", "3", ",", "14", ",", "9",
    ",", "1"}], "}"}]], "Output",
 CellChangeTimes->{3.756885739904084*^9},
 CellLabel->"Out[84]=",ExpressionUUID->"30a9d050-e460-4caa-a0c9-591677bf8b4e"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"pad2", " ", "=", " ", 
  RowBox[{"PadRight", "[", 
   RowBox[{"jlist", ",", " ", "11", ",", " ", "x"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.756885743252088*^9, 3.756885754309099*^9}},
 CellLabel->"In[85]:=",ExpressionUUID->"e1e6a7fa-9d28-4b5b-a5c6-b37982d57d89"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "6", ",", "18", ",", "3", ",", "14", ",", "9", ",", "1", ",", "x", ",", "x",
    ",", "x", ",", "x", ",", "x"}], "}"}]], "Output",
 CellChangeTimes->{3.756885755245081*^9},
 CellLabel->"Out[85]=",ExpressionUUID->"7fe7365a-f324-45a3-bd36-8015bf21868b"]
}, Open  ]]
},
WindowSize->{925, 988},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 378, 7, 28, "Input",ExpressionUUID->"60b9642a-b8a1-4b1d-b65d-e97c3231c7cb"],
Cell[961, 31, 271, 6, 32, "Output",ExpressionUUID->"12c8db34-d67a-49b9-aefb-e50d105fd5d2"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1269, 42, 282, 5, 28, "Input",ExpressionUUID->"b977018c-7789-44ba-a9ec-2aa47d196ccc"],
Cell[1554, 49, 271, 6, 32, "Output",ExpressionUUID->"771b4df9-a6b3-414d-a24e-c7e0c53a3d9d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1862, 60, 279, 5, 28, "Input",ExpressionUUID->"053fc3c5-f9b9-4b89-85cb-e25408d3eeec"],
Cell[2144, 67, 271, 6, 32, "Output",ExpressionUUID->"d977ead8-90c8-4f00-b693-47e62b6b62cb"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2452, 78, 335, 7, 28, "Input",ExpressionUUID->"ff229102-fec9-4de1-9f85-8a3233c2b890"],
Cell[2790, 87, 241, 5, 32, "Output",ExpressionUUID->"4132e7c6-18d8-4831-8a69-91fcbdf6214e"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3068, 97, 250, 4, 28, "Input",ExpressionUUID->"084850ff-d18b-48e2-b69f-eb13265712d5"],
Cell[3321, 103, 239, 5, 32, "Output",ExpressionUUID->"ae370c28-7af8-4516-bbfa-826f9c29b7c4"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3597, 113, 293, 5, 28, "Input",ExpressionUUID->"0bc36551-9084-401c-a06d-2ab64bb7a9b0"],
Cell[3893, 120, 273, 6, 32, "Output",ExpressionUUID->"30a9d050-e460-4caa-a0c9-591677bf8b4e"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4203, 131, 291, 5, 28, "Input",ExpressionUUID->"e1e6a7fa-9d28-4b5b-a5c6-b37982d57d89"],
Cell[4497, 138, 293, 6, 65, "Output",ExpressionUUID->"7fe7365a-f324-45a3-bd36-8015bf21868b"]
}, Open  ]]
}
]
*)

