(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      2777,         80]
NotebookOptionsPosition[      1957,         58]
NotebookOutlinePosition[      2298,         73]
CellTagsIndexPosition[      2255,         70]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{"14", " ", "*", " ", "7"}], "\[IndentingNewLine]", 
 RowBox[{"19", " ", "+", " ", "8"}], "\[IndentingNewLine]", 
 RowBox[{"27", " ", "+", " ", "11"}]}], "Input",
 CellChangeTimes->{{3.7569570356553206`*^9, 3.7569570498373165`*^9}},
 CellLabel->"In[46]:=",ExpressionUUID->"19c69a71-41f2-448a-aa81-c03c71b5a279"],

Cell[BoxData["98"], "Output",
 CellChangeTimes->{3.7569570510283227`*^9},
 CellLabel->"Out[46]=",ExpressionUUID->"66a329be-ba9a-49ac-9dde-f8128c6709ef"],

Cell[BoxData["27"], "Output",
 CellChangeTimes->{3.756957051031347*^9},
 CellLabel->"Out[47]=",ExpressionUUID->"878cde86-31d0-4f42-a543-d5bad8f8be55"],

Cell[BoxData["38"], "Output",
 CellChangeTimes->{3.7569570510373116`*^9},
 CellLabel->"Out[48]=",ExpressionUUID->"2d52f3e9-2224-43e2-95b0-867c4e17a753"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"14", "  ", "*", " ", "7"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"19", " ", "+", " ", "8"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{"27", " ", "*", " ", "11"}]}], "Input",
 CellChangeTimes->{{3.7569570696543183`*^9, 3.7569570822343187`*^9}},
 CellLabel->"In[49]:=",ExpressionUUID->"170285a5-0125-4d0c-b969-b9017226c55a"],

Cell[BoxData["297"], "Output",
 CellChangeTimes->{3.7569570832653136`*^9},
 CellLabel->"Out[51]=",ExpressionUUID->"70cf5652-b924-4786-bddf-037c05c25c56"]
}, Open  ]]
},
WindowSize->{636, 773},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 334, 5, 67, "Input",ExpressionUUID->"19c69a71-41f2-448a-aa81-c03c71b5a279"],
Cell[917, 29, 152, 2, 32, "Output",ExpressionUUID->"66a329be-ba9a-49ac-9dde-f8128c6709ef"],
Cell[1072, 33, 150, 2, 32, "Output",ExpressionUUID->"878cde86-31d0-4f42-a543-d5bad8f8be55"],
Cell[1225, 37, 152, 2, 32, "Output",ExpressionUUID->"2d52f3e9-2224-43e2-95b0-867c4e17a753"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1414, 44, 371, 7, 67, "Input",ExpressionUUID->"170285a5-0125-4d0c-b969-b9017226c55a"],
Cell[1788, 53, 153, 2, 65, "Output",ExpressionUUID->"70cf5652-b924-4786-bddf-037c05c25c56"]
}, Open  ]]
}
]
*)

