(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      1460,         51]
NotebookOptionsPosition[      1001,         34]
NotebookOutlinePosition[      1385,         50]
CellTagsIndexPosition[      1342,         47]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{
  RowBox[{"27", "^", "2"}], " ", "*", " ", "Pi"}]], "Input",
 CellChangeTimes->{{3.7568649587871*^9, 3.756864962582364*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"b294bf5f-897b-4b13-8ce7-1184c799ad84"],

Cell[BoxData[
 RowBox[{"729", " ", "\[Pi]"}]], "Output",
 CellChangeTimes->{3.756864963343341*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"544aacee-e4a6-483d-9f60-b933f5110a86"]
}, Open  ]]
},
WindowSize->{958, 988},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
PrintingPageRange->{Automatic, Automatic},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 226, 4, 28, "Input",ExpressionUUID->"b294bf5f-897b-4b13-8ce7-1184c799ad84"],
Cell[809, 28, 176, 3, 89, "Output",ExpressionUUID->"544aacee-e4a6-483d-9f60-b933f5110a86"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

