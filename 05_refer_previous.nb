(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      4415,        123]
NotebookOptionsPosition[      2941,         89]
NotebookOutlinePosition[      3282,        104]
CellTagsIndexPosition[      3239,        101]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{"803", " ", "201"}], "\[IndentingNewLine]", 
 RowBox[{"443", " ", "-", " ", "84"}], "\[IndentingNewLine]", 
 RowBox[{"113", " ", "^", " ", "2"}]}], "Input",
 CellChangeTimes->{{3.756865464863538*^9, 3.756865484241045*^9}},
 CellLabel->"In[16]:=",ExpressionUUID->"e806eb33-bce8-4b3a-8c23-74d8fd36b772"],

Cell[BoxData["161403"], "Output",
 CellChangeTimes->{3.7568654850550604`*^9},
 CellLabel->"Out[16]=",ExpressionUUID->"56e08226-7468-4a5d-858a-950fa7d4fea7"],

Cell[BoxData["359"], "Output",
 CellChangeTimes->{3.756865485058035*^9},
 CellLabel->"Out[17]=",ExpressionUUID->"c7a9fdc6-4c0c-475b-b355-29aa1b0edf27"],

Cell[BoxData["12769"], "Output",
 CellChangeTimes->{3.756865485060023*^9},
 CellLabel->"Out[18]=",ExpressionUUID->"db45b81d-8e3a-46ee-8f1f-b1063cf34817"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData["%"], "Input",
 CellChangeTimes->{3.756865583003888*^9},
 CellLabel->"In[19]:=",ExpressionUUID->"cf71567e-11c5-4ac9-b600-61b8741195d6"],

Cell[BoxData["12769"], "Output",
 CellChangeTimes->{3.756865583637883*^9},
 CellLabel->"Out[19]=",ExpressionUUID->"d0a07e01-7033-4eaa-a527-c3da256dec22"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData["%16"], "Input",
 CellChangeTimes->{{3.7568655860848885`*^9, 3.7568655904338827`*^9}},
 CellLabel->"In[20]:=",ExpressionUUID->"ebd4ce9b-ef30-403d-9393-c1e1f1745990"],

Cell[BoxData["161403"], "Output",
 CellChangeTimes->{3.7568655912608995`*^9},
 CellLabel->"Out[20]=",ExpressionUUID->"b11da786-eb1a-44ee-8a32-532f60703ddc"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Out", "[", "17", "]"}]], "Input",
 CellChangeTimes->{{3.756865599174899*^9, 3.7568656029338837`*^9}},
 CellLabel->"In[21]:=",ExpressionUUID->"19497eb5-6498-464a-8925-e2fae9ce9eb9"],

Cell[BoxData["359"], "Output",
 CellChangeTimes->{3.7568656034378815`*^9},
 CellLabel->"Out[21]=",ExpressionUUID->"81cec745-48e8-41ab-a36f-77598d7ec2b8"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Out", "[", "16", "]"}], "  ", "*", " ", ".15"}]], "Input",
 CellChangeTimes->{{3.7568656246988845`*^9, 3.756865639478278*^9}},
 CellLabel->"In[22]:=",ExpressionUUID->"60001943-c5be-4223-9f25-3ecfaba05745"],

Cell[BoxData["24210.45`"], "Output",
 CellChangeTimes->{3.7568656398003035`*^9},
 CellLabel->"Out[22]=",ExpressionUUID->"7aff58a3-4b7e-449e-b894-ca49b77a4add"]
}, Open  ]]
},
WindowSize->{965, 988},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 325, 5, 67, "Input",ExpressionUUID->"e806eb33-bce8-4b3a-8c23-74d8fd36b772"],
Cell[908, 29, 156, 2, 32, "Output",ExpressionUUID->"56e08226-7468-4a5d-858a-950fa7d4fea7"],
Cell[1067, 33, 151, 2, 32, "Output",ExpressionUUID->"c7a9fdc6-4c0c-475b-b355-29aa1b0edf27"],
Cell[1221, 37, 153, 2, 32, "Output",ExpressionUUID->"db45b81d-8e3a-46ee-8f1f-b1063cf34817"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1411, 44, 148, 2, 28, "Input",ExpressionUUID->"cf71567e-11c5-4ac9-b600-61b8741195d6"],
Cell[1562, 48, 153, 2, 32, "Output",ExpressionUUID->"d0a07e01-7033-4eaa-a527-c3da256dec22"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1752, 55, 178, 2, 28, "Input",ExpressionUUID->"ebd4ce9b-ef30-403d-9393-c1e1f1745990"],
Cell[1933, 59, 156, 2, 32, "Output",ExpressionUUID->"b11da786-eb1a-44ee-8a32-532f60703ddc"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2126, 66, 204, 3, 28, "Input",ExpressionUUID->"19497eb5-6498-464a-8925-e2fae9ce9eb9"],
Cell[2333, 71, 153, 2, 32, "Output",ExpressionUUID->"81cec745-48e8-41ab-a36f-77598d7ec2b8"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2523, 78, 240, 4, 28, "Input",ExpressionUUID->"60001943-c5be-4223-9f25-3ecfaba05745"],
Cell[2766, 84, 159, 2, 65, "Output",ExpressionUUID->"7aff58a3-4b7e-449e-b894-ca49b77a4add"]
}, Open  ]]
}
]
*)

