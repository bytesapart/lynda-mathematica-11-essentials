(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      6301,        171]
NotebookOptionsPosition[      5441,        147]
NotebookOutlinePosition[      5782,        162]
CellTagsIndexPosition[      5739,        159]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"alist", " ", "=", " ", 
  RowBox[{
  "Import", "[", 
   "\"\<C:\\\\Users\\\\Osama Iqbal\\\\Google Drive \
(osama@bytesapart.com)\\\\MathematicaTutorials\\\\lynda-mathematica-11-\
essentials\\\\Create.csv\>\"", "]"}]}]], "Input",
 CellChangeTimes->{{3.7568658313312435`*^9, 3.7568658375602384`*^9}, {
  3.756866437592774*^9, 3.756866442147801*^9}},
 CellLabel->"In[27]:=",ExpressionUUID->"e3d02052-94b7-4d8d-8a08-2b8f414dcefd"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"\<\"Cambridge\"\>", ",", "1", ",", "51992"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Cambridge\"\>", ",", "2", ",", "45427"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Cambridge\"\>", ",", "3", ",", "38092"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Cambridge\"\>", ",", "4", ",", "33519"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Oxford\"\>", ",", "1", ",", "70127"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Oxford\"\>", ",", "2", ",", "54718"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Oxford\"\>", ",", "3", ",", "27576"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Oxford\"\>", ",", "4", ",", "26001"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Piccadilly\"\>", ",", "1", ",", "17579"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Piccadilly\"\>", ",", "2", ",", "23392"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Piccadilly\"\>", ",", "3", ",", "15770"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Piccadilly\"\>", ",", "4", ",", "64154"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Stratham\"\>", ",", "1", ",", "21909"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Stratham\"\>", ",", "2", ",", "55578"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Stratham\"\>", ",", "3", ",", "64046"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Stratham\"\>", ",", "4", ",", "46200"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Westminster\"\>", ",", "1", ",", "64682"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Westminster\"\>", ",", "2", ",", "20283"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Westminster\"\>", ",", "3", ",", "51359"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"\<\"Westminster\"\>", ",", "4", ",", "28976"}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{3.75686644828384*^9},
 CellLabel->"Out[27]=",ExpressionUUID->"ec4a1c69-be21-4178-8d7b-d324a30b9ced"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"TableForm", "[", "alist", "]"}]], "Input",
 CellChangeTimes->{{3.7568664550478387`*^9, 3.7568664584888554`*^9}},
 CellLabel->"In[28]:=",ExpressionUUID->"6cd5fba0-d4fc-429c-9f19-fc378e85bda7"],

Cell[BoxData[
 TagBox[GridBox[{
    {"\<\"Cambridge\"\>", "1", "51992"},
    {"\<\"Cambridge\"\>", "2", "45427"},
    {"\<\"Cambridge\"\>", "3", "38092"},
    {"\<\"Cambridge\"\>", "4", "33519"},
    {"\<\"Oxford\"\>", "1", "70127"},
    {"\<\"Oxford\"\>", "2", "54718"},
    {"\<\"Oxford\"\>", "3", "27576"},
    {"\<\"Oxford\"\>", "4", "26001"},
    {"\<\"Piccadilly\"\>", "1", "17579"},
    {"\<\"Piccadilly\"\>", "2", "23392"},
    {"\<\"Piccadilly\"\>", "3", "15770"},
    {"\<\"Piccadilly\"\>", "4", "64154"},
    {"\<\"Stratham\"\>", "1", "21909"},
    {"\<\"Stratham\"\>", "2", "55578"},
    {"\<\"Stratham\"\>", "3", "64046"},
    {"\<\"Stratham\"\>", "4", "46200"},
    {"\<\"Westminster\"\>", "1", "64682"},
    {"\<\"Westminster\"\>", "2", "20283"},
    {"\<\"Westminster\"\>", "3", "51359"},
    {"\<\"Westminster\"\>", "4", "28976"}
   },
   GridBoxAlignment->{
    "Columns" -> {{Left}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
     "RowsIndexed" -> {}},
   GridBoxSpacings->{"Columns" -> {
       Offset[0.27999999999999997`], {
        Offset[2.0999999999999996`]}, 
       Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
       Offset[0.2], {
        Offset[0.4]}, 
       Offset[0.2]}, "RowsIndexed" -> {}}],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.7568664604398413`*^9},
 CellLabel->
  "Out[28]//TableForm=",ExpressionUUID->"9b7e4204-b9af-4787-9ed7-\
73d6bd430ca1"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Export", "[", 
  RowBox[{
  "\"\<C:\\\\Users\\\\Osama Iqbal\\\\Google Drive \
(osama@bytesapart.com)\\\\MathematicaTutorials\\\\lynda-mathematica-11-\
essentials\\\\export.csv\>\"", ",", " ", "CSV"}], "]"}]], "Input",
 CellChangeTimes->{{3.7568664807708397`*^9, 3.7568665030118575`*^9}},
 CellLabel->"In[29]:=",ExpressionUUID->"716ee2ef-9fef-45ac-999c-7b30036bd026"],

Cell[BoxData["\<\"C:\\\\Users\\\\Osama Iqbal\\\\Google Drive \
(osama@bytesapart.com)\\\\MathematicaTutorials\\\\lynda-mathematica-11-\
essentials\\\\export.csv\"\>"], "Output",
 CellChangeTimes->{3.756866507401841*^9},
 CellLabel->"Out[29]=",ExpressionUUID->"a1f43c91-6a31-46fb-a86f-41fa1d31fda1"]
}, Open  ]]
},
WindowSize->{965, 988},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 450, 9, 86, "Input",ExpressionUUID->"e3d02052-94b7-4d8d-8a08-2b8f414dcefd"],
Cell[1033, 33, 1952, 45, 90, "Output",ExpressionUUID->"ec4a1c69-be21-4178-8d7b-d324a30b9ced"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3022, 83, 215, 3, 28, "Input",ExpressionUUID->"6cd5fba0-d4fc-429c-9f19-fc378e85bda7"],
Cell[3240, 88, 1457, 38, 368, "Output",ExpressionUUID->"9b7e4204-b9af-4787-9ed7-73d6bd430ca1"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4734, 131, 390, 7, 67, "Input",ExpressionUUID->"716ee2ef-9fef-45ac-999c-7b30036bd026"],
Cell[5127, 140, 298, 4, 89, "Output",ExpressionUUID->"a1f43c91-6a31-46fb-a86f-41fa1d31fda1"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

