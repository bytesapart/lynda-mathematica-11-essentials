(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      3244,         97]
NotebookOptionsPosition[      2175,         69]
NotebookOutlinePosition[      2516,         84]
CellTagsIndexPosition[      2473,         81]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"rate", " ", "=", " ", "0.19"}]], "Input",
 CellChangeTimes->{{3.7568657258232403`*^9, 3.75686572789124*^9}},
 CellLabel->"In[23]:=",ExpressionUUID->"85c6f4be-ec0a-4d08-a92d-3c0efe41cb87"],

Cell[BoxData["0.19`"], "Output",
 CellChangeTimes->{3.7568657286252365`*^9},
 CellLabel->"Out[23]=",ExpressionUUID->"f2760d92-e675-47ba-8b46-e3365c6a1c07"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"500", " ", "rate"}]], "Input",
 CellChangeTimes->{{3.7568657341932383`*^9, 3.756865740533253*^9}},
 CellLabel->"In[24]:=",ExpressionUUID->"1af06ec5-3880-4e0a-81bd-3568d96a8663"],

Cell[BoxData["95.`"], "Output",
 CellChangeTimes->{3.756865742077239*^9},
 CellLabel->"Out[24]=",ExpressionUUID->"300a1ec3-b4af-463a-ad68-fd15d2f982e4"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"500", " ", "Rate"}]], "Input",
 CellChangeTimes->{{3.756865748318257*^9, 3.75686575212624*^9}},
 CellLabel->"In[25]:=",ExpressionUUID->"71c4dc95-9ec6-4942-918a-e3d27411d330"],

Cell[BoxData[
 RowBox[{"500", " ", "Rate"}]], "Output",
 CellChangeTimes->{3.756865753166239*^9},
 CellLabel->"Out[25]=",ExpressionUUID->"2f59bd00-9f47-4171-8849-764b09f632f5"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"200", " ", "*", " ", "rate"}]], "Input",
 CellChangeTimes->{{3.756865762578245*^9, 3.756865765557238*^9}},
 CellLabel->"In[26]:=",ExpressionUUID->"9808465d-d930-495d-a8a8-6e6baaa7c85c"],

Cell[BoxData["38.`"], "Output",
 CellChangeTimes->{3.7568657671262436`*^9},
 CellLabel->"Out[26]=",ExpressionUUID->"6a9c2c5c-da85-474e-9d49-d50e94cc5b28"]
}, Open  ]]
},
WindowSize->{965, 988},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 211, 3, 28, "Input",ExpressionUUID->"85c6f4be-ec0a-4d08-a92d-3c0efe41cb87"],
Cell[794, 27, 155, 2, 32, "Output",ExpressionUUID->"f2760d92-e675-47ba-8b46-e3365c6a1c07"]
}, Open  ]],
Cell[CellGroupData[{
Cell[986, 34, 201, 3, 28, "Input",ExpressionUUID->"1af06ec5-3880-4e0a-81bd-3568d96a8663"],
Cell[1190, 39, 152, 2, 32, "Output",ExpressionUUID->"300a1ec3-b4af-463a-ad68-fd15d2f982e4"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1379, 46, 198, 3, 28, "Input",ExpressionUUID->"71c4dc95-9ec6-4942-918a-e3d27411d330"],
Cell[1580, 51, 176, 3, 32, "Output",ExpressionUUID->"2f59bd00-9f47-4171-8849-764b09f632f5"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1793, 59, 209, 3, 28, "Input",ExpressionUUID->"9808465d-d930-495d-a8a8-6e6baaa7c85c"],
Cell[2005, 64, 154, 2, 65, "Output",ExpressionUUID->"6a9c2c5c-da85-474e-9d49-d50e94cc5b28"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

