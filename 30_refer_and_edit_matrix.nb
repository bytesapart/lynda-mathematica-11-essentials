(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      9101,        273]
NotebookOptionsPosition[      6927,        225]
NotebookOutlinePosition[      7268,        240]
CellTagsIndexPosition[      7225,        237]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"amat", " ", "=", " ", 
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"7", "14", "21"},
     {"8", "9", "10"}
    }], "\[NoBreak]", ")"}]}]], "Input",
 CellChangeTimes->{{3.75695668889931*^9, 3.7569567172853174`*^9}},
 CellLabel->"In[34]:=",ExpressionUUID->"a246f450-247a-4f8f-90d4-e0def66fa64f"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"7", ",", "14", ",", "21"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"8", ",", "9", ",", "10"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.756956719897318*^9},
 CellLabel->"Out[34]=",ExpressionUUID->"ceaf237d-d2b1-4124-8123-b6316679617f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"amat", "[", 
  RowBox[{"[", 
   RowBox[{"1", ",", " ", "2"}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.7569567317493176`*^9, 3.7569567360373173`*^9}},
 CellLabel->"In[35]:=",ExpressionUUID->"0d6e78d5-ffe3-41ef-b624-b7846873e949"],

Cell[BoxData["14"], "Output",
 CellChangeTimes->{3.7569567364673033`*^9},
 CellLabel->"Out[35]=",ExpressionUUID->"32216771-d6ae-46b8-aac8-20efa14d77da"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"amat", "[", 
  RowBox[{"[", "All", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.7569567490720243`*^9, 3.756956754409476*^9}},
 CellLabel->"In[36]:=",ExpressionUUID->"d6bd12f8-8039-4e4f-a115-5b5c5eed1032"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"7", ",", "14", ",", "21"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"8", ",", "9", ",", "10"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.756956754697472*^9},
 CellLabel->"Out[36]=",ExpressionUUID->"df8c1016-bf4c-49bd-988c-5ba267b06459"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"amat", "[", 
   RowBox[{"[", "All", "]"}], "]"}], " ", "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.756956757823488*^9, 3.7569567619004946`*^9}},
 CellLabel->"In[37]:=",ExpressionUUID->"249546fb-85f9-47b1-88dd-3cc331339d8f"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"7", "14", "21"},
     {"8", "9", "10"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.7569567624674735`*^9},
 CellLabel->
  "Out[37]//MatrixForm=",ExpressionUUID->"26cc1518-63cc-4be0-bf69-\
c977247f679d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"amat", "[", 
  RowBox[{"[", 
   RowBox[{"2", ",", " ", "All"}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.7569567725944777`*^9, 3.756956781451473*^9}},
 CellLabel->"In[38]:=",ExpressionUUID->"55c00179-c5b9-492b-b478-5cfde26d50f8"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"8", ",", "9", ",", "10"}], "}"}]], "Output",
 CellChangeTimes->{3.756956781895482*^9},
 CellLabel->"Out[38]=",ExpressionUUID->"4747438e-4953-4d54-93a0-e32acea80f7f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"amat", "[", 
   RowBox[{"[", 
    RowBox[{"2", ",", " ", "All"}], "]"}], "]"}], " ", "//", " ", 
  "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.7569567827155085`*^9, 3.756956790017498*^9}},
 CellLabel->"In[39]:=",ExpressionUUID->"91c509ad-be90-42b8-b2eb-38596abd2835"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", 
   TagBox[GridBox[{
      {"8"},
      {"9"},
      {"10"}
     },
     GridBoxAlignment->{
      "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}},
        "RowsIndexed" -> {}},
     GridBoxSpacings->{"Columns" -> {
         Offset[0.27999999999999997`], {
          Offset[0.5599999999999999]}, 
         Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
         Offset[0.2], {
          Offset[0.4]}, 
         Offset[0.2]}, "RowsIndexed" -> {}}],
    Column], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.756956790446481*^9},
 CellLabel->
  "Out[39]//MatrixForm=",ExpressionUUID->"0302dfe7-bf80-4e89-af38-\
31ce0a2cf1a1"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"amat", "[", 
  RowBox[{"[", 
   RowBox[{"1", ",", 
    RowBox[{"1", ";;", "2"}]}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.7569568063024893`*^9, 3.7569568161924877`*^9}},
 CellLabel->"In[40]:=",ExpressionUUID->"677ac6e0-1260-434f-9c88-0631ff6fe8d9"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"7", ",", "14"}], "}"}]], "Output",
 CellChangeTimes->{3.7569568165844903`*^9},
 CellLabel->"Out[40]=",ExpressionUUID->"483bc7ba-8854-4959-8bcd-569e832b403c"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"amat", "[", 
   RowBox[{"[", 
    RowBox[{"1", ",", "1"}], "]"}], "]"}], " ", "=", " ", "0"}]], "Input",
 CellChangeTimes->{{3.756956823432479*^9, 3.7569568289584785`*^9}},
 CellLabel->"In[41]:=",ExpressionUUID->"61c0770c-9f26-454f-80a2-53a042dde0c2"],

Cell[BoxData["0"], "Output",
 CellChangeTimes->{3.75695682940449*^9},
 CellLabel->"Out[41]=",ExpressionUUID->"49acfe9c-f540-4240-a94e-b7ed2cfe8110"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"amat", " ", "//", " ", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.7569568309224806`*^9, 3.7569568328414803`*^9}},
 CellLabel->"In[42]:=",ExpressionUUID->"465fffee-fd4f-4274-af07-f5ebef0014a9"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"0", "14", "21"},
     {"8", "9", "10"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.756956833180479*^9},
 CellLabel->
  "Out[42]//MatrixForm=",ExpressionUUID->"4c396621-dd93-4972-a000-\
27b2bce1f7f6"]
}, Open  ]]
},
WindowSize->{636, 773},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 318, 7, 44, "Input",ExpressionUUID->"a246f450-247a-4f8f-90d4-e0def66fa64f"],
Cell[901, 31, 315, 8, 32, "Output",ExpressionUUID->"ceaf237d-d2b1-4124-8123-b6316679617f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1253, 44, 258, 5, 28, "Input",ExpressionUUID->"0d6e78d5-ffe3-41ef-b624-b7846873e949"],
Cell[1514, 51, 152, 2, 32, "Output",ExpressionUUID->"32216771-d6ae-46b8-aac8-20efa14d77da"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1703, 58, 229, 4, 28, "Input",ExpressionUUID->"d6bd12f8-8039-4e4f-a115-5b5c5eed1032"],
Cell[1935, 64, 315, 8, 32, "Output",ExpressionUUID->"df8c1016-bf4c-49bd-988c-5ba267b06459"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2287, 77, 268, 5, 28, "Input",ExpressionUUID->"249546fb-85f9-47b1-88dd-3cc331339d8f"],
Cell[2558, 84, 738, 21, 62, "Output",ExpressionUUID->"26cc1518-63cc-4be0-bf69-c977247f679d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3333, 110, 258, 5, 28, "Input",ExpressionUUID->"55c00179-c5b9-492b-b478-5cfde26d50f8"],
Cell[3594, 117, 205, 4, 32, "Output",ExpressionUUID->"4747438e-4953-4d54-93a0-e32acea80f7f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3836, 126, 306, 7, 28, "Input",ExpressionUUID->"91c509ad-be90-42b8-b2eb-38596abd2835"],
Cell[4145, 135, 779, 24, 79, "Output",ExpressionUUID->"0302dfe7-bf80-4e89-af38-31ce0a2cf1a1"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4961, 164, 279, 6, 28, "Input",ExpressionUUID->"677ac6e0-1260-434f-9c88-0631ff6fe8d9"],
Cell[5243, 172, 197, 4, 32, "Output",ExpressionUUID->"483bc7ba-8854-4959-8bcd-569e832b403c"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5477, 181, 286, 6, 28, "Input",ExpressionUUID->"61c0770c-9f26-454f-80a2-53a042dde0c2"],
Cell[5766, 189, 148, 2, 32, "Output",ExpressionUUID->"49acfe9c-f540-4240-a94e-b7ed2cfe8110"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5951, 196, 221, 3, 28, "Input",ExpressionUUID->"465fffee-fd4f-4274-af07-f5ebef0014a9"],
Cell[6175, 201, 736, 21, 119, "Output",ExpressionUUID->"4c396621-dd93-4972-a000-27b2bce1f7f6"]
}, Open  ]]
}
]
*)

