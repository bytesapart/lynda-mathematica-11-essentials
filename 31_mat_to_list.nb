(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      2434,         81]
NotebookOptionsPosition[      1799,         61]
NotebookOutlinePosition[      2140,         76]
CellTagsIndexPosition[      2097,         73]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"mat1", " ", "=", " ", 
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"1", "4"},
     {"2", "5"},
     {"3", "6"}
    }], "\[NoBreak]", ")"}]}]], "Input",
 CellChangeTimes->{{3.756956917986487*^9, 3.7569569514654837`*^9}},
 CellLabel->"In[43]:=",ExpressionUUID->"a20441e2-4b4d-4bcc-8a0c-8f8a01d6f411"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "4"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"2", ",", "5"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"3", ",", "6"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.7569569538934865`*^9},
 CellLabel->"Out[43]=",ExpressionUUID->"0704a1d9-f60d-40aa-b484-b60d0d904e06"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"list1", " ", "=", " ", 
  RowBox[{"Flatten", "[", "mat1", "]"}]}]], "Input",
 CellChangeTimes->{{3.7569569764556365`*^9, 3.756956984065635*^9}},
 CellLabel->"In[44]:=",ExpressionUUID->"2b223078-42ef-4428-9d04-ebc29b4ab5ed"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"1", ",", "4", ",", "2", ",", "5", ",", "3", ",", "6"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.756956984584626*^9},
 CellLabel->"Out[44]=",ExpressionUUID->"5abde78f-5079-435a-bdaf-8942b3be5e31"]
}, Open  ]]
},
WindowSize->{636, 773},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 323, 8, 61, "Input",ExpressionUUID->"a20441e2-4b4d-4bcc-8a0c-8f8a01d6f411"],
Cell[906, 32, 353, 10, 32, "Output",ExpressionUUID->"0704a1d9-f60d-40aa-b484-b60d0d904e06"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1296, 47, 247, 4, 28, "Input",ExpressionUUID->"2b223078-42ef-4428-9d04-ebc29b4ab5ed"],
Cell[1546, 53, 237, 5, 89, "Output",ExpressionUUID->"5abde78f-5079-435a-bdaf-8942b3be5e31"]
}, Open  ]]
}
]
*)

