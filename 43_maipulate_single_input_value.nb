(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      6064,        162]
NotebookOptionsPosition[      5420,        142]
NotebookOutlinePosition[      5761,        157]
CellTagsIndexPosition[      5718,        154]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"Manipulate", "[", 
  RowBox[{
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"x", "^", "3"}], "-", 
      RowBox[{"a", " ", 
       RowBox[{"x", "^", "2"}]}], "+", "a"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"x", ",", " ", 
       RowBox[{"-", "6"}], ",", " ", "6"}], "}"}]}], "]"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"a", ",", " ", 
     RowBox[{"-", "15"}], ",", " ", "25"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.7569590018725758`*^9, 3.7569590350105963`*^9}, 
   3.756959068202609*^9},
 CellLabel->
  "In[100]:=",ExpressionUUID->"c2fdf6e2-f17a-4472-8437-b42cfdac7f75"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`a$$ = -15., Typeset`show$$ = True, 
    Typeset`bookmarkList$$ = {}, Typeset`bookmarkMode$$ = "Menu", 
    Typeset`animator$$, Typeset`animvar$$ = 1, Typeset`name$$ = 
    "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`a$$], -15, 25}}, Typeset`size$$ = {
    360., {109., 115.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True, $CellContext`a$58039$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`a$$ = -15}, 
      "ControllerVariables" :> {
        Hold[$CellContext`a$$, $CellContext`a$58039$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> 
      Plot[$CellContext`x^3 - $CellContext`a$$ $CellContext`x^2 + \
$CellContext`a$$, {$CellContext`x, -6, 6}], 
      "Specifications" :> {{$CellContext`a$$, -15, 25}}, "Options" :> {}, 
      "DefaultOptions" :> {}],
     ImageSizeCache->{411., {169., 175.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{{3.756959039863593*^9, 3.756959072981619*^9}},
 CellLabel->
  "Out[100]=",ExpressionUUID->"bb44c917-be61-4fe2-b96b-a5d9fc125bb1"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Manipulate", "[", 
  RowBox[{
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"x", "^", "3"}], "-", 
      RowBox[{"a", " ", 
       RowBox[{"x", "^", "2"}]}], "+", "a"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"x", ",", " ", 
       RowBox[{"-", "6"}], ",", " ", "6"}], "}"}]}], "]"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"a", ",", " ", 
     RowBox[{"-", "15"}], ",", " ", "25", ",", " ", "5"}], "}"}]}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.756959138213892*^9, 3.7569591425118933`*^9}},
 CellLabel->
  "In[101]:=",ExpressionUUID->"75b26ae2-4bf4-400b-be37-2e477687b7c0"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`a$$ = 20, Typeset`show$$ = True, 
    Typeset`bookmarkList$$ = {}, Typeset`bookmarkMode$$ = "Menu", 
    Typeset`animator$$, Typeset`animvar$$ = 1, Typeset`name$$ = 
    "\"untitled\"", Typeset`specs$$ = {{
      Hold[$CellContext`a$$], -15, 25, 5}}, Typeset`size$$ = {
    360., {109., 114.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = True, $CellContext`a$75782$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, "Variables" :> {$CellContext`a$$ = -15}, 
      "ControllerVariables" :> {
        Hold[$CellContext`a$$, $CellContext`a$75782$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> 
      Plot[$CellContext`x^3 - $CellContext`a$$ $CellContext`x^2 + \
$CellContext`a$$, {$CellContext`x, -6, 6}], 
      "Specifications" :> {{$CellContext`a$$, -15, 25, 5}}, "Options" :> {}, 
      "DefaultOptions" :> {}],
     ImageSizeCache->{411., {169., 175.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 CellChangeTimes->{{3.75695914374693*^9, 3.756959146056928*^9}},
 CellLabel->
  "Out[101]=",ExpressionUUID->"aef4382c-e50f-4f4e-af5a-b20b97f91d35"]
}, Open  ]]
},
WindowSize->{636, 773},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 638, 18, 28, "Input",ExpressionUUID->"c2fdf6e2-f17a-4472-8437-b42cfdac7f75"],
Cell[1221, 42, 1756, 36, 363, "Output",ExpressionUUID->"bb44c917-be61-4fe2-b96b-a5d9fc125bb1"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3014, 83, 628, 18, 28, "Input",ExpressionUUID->"75b26ae2-4bf4-400b-be37-2e477687b7c0"],
Cell[3645, 103, 1759, 36, 396, "Output",ExpressionUUID->"aef4382c-e50f-4f4e-af5a-b20b97f91d35"]
}, Open  ]]
}
]
*)

