(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     29482,        795]
NotebookOptionsPosition[     27949,        759]
NotebookOutlinePosition[     28290,        774]
CellTagsIndexPosition[     28247,        771]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"alist", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{"4500", ",", " ", "3000", ",", " ", "2150", ",", " ", "6879"}], 
   "}"}]}]], "Input",
 CellChangeTimes->{{3.7569576459836116`*^9, 3.7569576589156294`*^9}},
 CellLabel->"In[75]:=",ExpressionUUID->"9ae5341c-9fc5-4ee2-8971-e89cee6cf00d"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"4500", ",", "3000", ",", "2150", ",", "6879"}], "}"}]], "Output",
 CellChangeTimes->{3.7569576596158047`*^9},
 CellLabel->"Out[75]=",ExpressionUUID->"c3668c09-653e-4b03-beda-29e457d9b7ab"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"BarChart", "[", "alist", "]"}]], "Input",
 CellChangeTimes->{{3.756957662479986*^9, 3.756957667043994*^9}},
 CellLabel->"In[76]:=",ExpressionUUID->"38484e62-833c-41d4-aa52-772ce79ff3af"],

Cell[BoxData[
 GraphicsBox[{
   {Opacity[0], 
    PointBox[{{0.0195484340859432, 0.}, {4.902403495994173, 0.}}]}, {{}, 
    {RGBColor[0.982864, 0.7431472, 0.3262672], EdgeForm[{Opacity[
     0.6719999999999999], Thickness[Small]}], 
     {RGBColor[0.982864, 0.7431472, 0.3262672], EdgeForm[{Opacity[
      0.6719999999999999], Thickness[Small]}], 
      TagBox[
       TooltipBox[
        TagBox[
         DynamicBox[{
           FEPrivate`If[
            CurrentValue["MouseOver"], 
            EdgeForm[{
              GrayLevel[0.5], 
              AbsoluteThickness[1.5], 
              Opacity[0.66]}], {}, {}], 
           
           RectangleBox[{0.5487982520029133, 0.}, {1.4512017479970867`, 
            4500.}, "RoundingRadius" -> 0]},
          
          ImageSizeCache->{{61.372430461162296`, 
           120.90580417291206`}, {-30.940743605851267`, 96.93728426496511}}],
         StatusArea[#, 4500]& ,
         TagBoxNote->"4500"],
        StyleBox["4500", {}, StripOnInput -> False]],
       Annotation[#, 
        Style[4500, {}], "Tooltip"]& ]}, 
     {RGBColor[0.982864, 0.7431472, 0.3262672], EdgeForm[{Opacity[
      0.6719999999999999], Thickness[Small]}], 
      TagBox[
       TooltipBox[
        TagBox[
         DynamicBox[{
           FEPrivate`If[
            CurrentValue["MouseOver"], 
            EdgeForm[{
              GrayLevel[0.5], 
              AbsoluteThickness[1.5], 
              Opacity[0.66]}], {}, {}], 
           
           RectangleBox[{1.5487982520029133`, 0.}, {2.4512017479970867`, 
            3000.}, "RoundingRadius" -> 0]},
          
          ImageSizeCache->{{126.79036597870258`, 186.32373969045233`}, {
           11.518599017754184`, 96.93728426496511}}],
         StatusArea[#, 3000]& ,
         TagBoxNote->"3000"],
        StyleBox["3000", {}, StripOnInput -> False]],
       Annotation[#, 
        Style[3000, {}], "Tooltip"]& ]}, 
     {RGBColor[0.982864, 0.7431472, 0.3262672], EdgeForm[{Opacity[
      0.6719999999999999], Thickness[Small]}], 
      TagBox[
       TooltipBox[
        TagBox[
         DynamicBox[{
           FEPrivate`If[
            CurrentValue["MouseOver"], 
            EdgeForm[{
              GrayLevel[0.5], 
              AbsoluteThickness[1.5], 
              Opacity[0.66]}], {}, {}], 
           
           RectangleBox[{2.5487982520029133`, 0.}, {3.4512017479970867`, 
            2150.}, "RoundingRadius" -> 0]},
          
          ImageSizeCache->{{192.20830149624288`, 251.7416752079926}, {
           35.578893171130616`, 96.93728426496511}}],
         StatusArea[#, 2150]& ,
         TagBoxNote->"2150"],
        StyleBox["2150", {}, StripOnInput -> False]],
       Annotation[#, 
        Style[2150, {}], "Tooltip"]& ]}, 
     {RGBColor[0.982864, 0.7431472, 0.3262672], EdgeForm[{Opacity[
      0.6719999999999999], Thickness[Small]}], 
      TagBox[
       TooltipBox[
        TagBox[
         DynamicBox[{
           FEPrivate`If[
            CurrentValue["MouseOver"], 
            EdgeForm[{
              GrayLevel[0.5], 
              AbsoluteThickness[1.5], 
              Opacity[0.66]}], {}, {}], 
           
           RectangleBox[{3.5487982520029133`, 0.}, {4.451201747997087, 6879.},
             "RoundingRadius" -> 0]},
          
          ImageSizeCache->{{257.62623701378317`, 
           317.1596107255329}, {-98.28126100688954, 96.93728426496511}}],
         StatusArea[#, 6879]& ,
         TagBoxNote->"6879"],
        StyleBox["6879", {}, StripOnInput -> False]],
       Annotation[#, 
        Style[6879, {}], "Tooltip"]& ]}}, {}, {}}, {}, {}, {}, {}, 
   StyleBox[
    StyleBox[{
      {Thickness[Tiny], 
       LineBox[{{0.0195484340859432, 0.}, {4.991036562272396, 0.}}], 
       StyleBox[{}, "GraphicsLabel",
        StripOnInput->False]}, 
      StyleBox[{
        {Thickness[Tiny], 
         LineBox[{{0.5487982520029133, 0.}, 
           Offset[{-1.102182119232618*^-15, -6.}, {0.5487982520029133, 0.}]}],
          LineBox[{{4.451201747997087, 0.}, 
           Offset[{-1.102182119232618*^-15, -6.}, {4.451201747997087, 
            0.}]}], {{}, {}, {}, {}, {}, {}}}, 
        StyleBox[{}, "GraphicsLabel",
         StripOnInput->False]}, "GraphicsTicks",
       StripOnInput->False]},
     Antialiasing->False], "GraphicsAxes",
    StripOnInput->False]},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{False, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.0195484340859432, 0.},
  CoordinatesToolOptions:>{"DisplayFunction" -> ({
      Identity[
       Part[#, 1]], 
      Identity[
       Part[#, 2]]}& ), "CopiedValueFunction" -> ({
      Identity[
       Part[#, 1]], 
      Identity[
       Part[#, 2]]}& )},
  DisplayFunction->Identity,
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  PlotRange->{{All, All}, {All, All}},
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{None, Automatic}]], "Output",
 CellChangeTimes->{3.756957668350995*^9},
 CellLabel->"Out[76]=",ExpressionUUID->"f21c34db-7416-4ee1-8ce4-d663bab0626c"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"blist", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"4500", ",", " ", "3000", ",", " ", "2150", ",", " ", "6879"}], 
     "}"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"6200", ",", " ", "1400", ",", " ", "2200", ",", " ", "8000"}], 
     "}"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.7569577396096287`*^9, 3.7569577735536184`*^9}},
 CellLabel->"In[77]:=",ExpressionUUID->"b988eef9-3613-4651-bcce-ed3453345743"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"4500", ",", "3000", ",", "2150", ",", "6879"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"6200", ",", "1400", ",", "2200", ",", "8000"}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{3.756957774368617*^9},
 CellLabel->"Out[77]=",ExpressionUUID->"b3cf0d3d-87ea-4537-b415-8cfaa96f0fb9"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"BarChart", "[", "blist", "]"}]], "Input",
 CellChangeTimes->{{3.756957776822624*^9, 3.756957781722625*^9}},
 CellLabel->"In[78]:=",ExpressionUUID->"fd24ca15-8005-4f8f-a956-af869f14395a"],

Cell[BoxData[
 GraphicsBox[{
   {Opacity[0], 
    PointBox[{{0.5800873998543336, 0.}, {8.939184268026219, 
     0.}}]}, {{}, {{{}, {{}, {
        {RGBColor[0.982864, 0.7431472, 0.3262672], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{0.7439912600145666, 0.}, {1.64639475600874, 
               4500.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{33.001912529717764`, 
              67.92299155709208}, {-13.091897450453104`, 96.93728426496513}}],
            StatusArea[#, 4500]& ,
            TagBoxNote->"4500"],
           StyleBox["4500", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[4500, {}], "Tooltip"]& ]}, 
        {RGBColor[0.928, 0.5210666666666667, 0.2], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{1.7439912600145666`, 0.}, {2.64639475600874, 
               3000.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{71.14569098378146, 106.06677001115577`}, {
              23.41782978801963, 96.93728426496513}}],
            StatusArea[#, 3000]& ,
            TagBoxNote->"3000"],
           StyleBox["3000", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[3000, {}], "Tooltip"]& ]}, 
        {RGBColor[0.7116405333333333, 0.4816, 0.5483194666666666], EdgeForm[{
         Opacity[0.644], Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{2.7439912600145666`, 0.}, {3.64639475600874, 
               2150.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{109.28946943784516`, 144.2105484652195}, {
              44.10667522315419, 96.93728426496513}}],
            StatusArea[#, 2150]& ,
            TagBoxNote->"2150"],
           StyleBox["2150", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[2150, {}], "Tooltip"]& ]}, 
        {RGBColor[0.4992, 0.5552, 0.8309304], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{3.7439912600145666`, 0.}, {4.64639475600874, 
               6879.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{147.43324789190888`, 
              182.35432691928318`}, {-70.99632485067089, 96.93728426496513}}],
            StatusArea[#, 6879]& ,
            TagBoxNote->"6879"],
           StyleBox["6879", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[6879, {}], "Tooltip"]& ]}}}, {}, {}}, {{}, {{}, {
        {RGBColor[0.982864, 0.7431472, 0.3262672], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{5.0367807720320465`, 0.}, {5.93918426802622, 
               6200.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{196.74512462604073`, 
              231.66620365341504`}, {-54.469588320722224`, 
              96.93728426496513}}],
            StatusArea[#, 6200]& ,
            TagBoxNote->"6200"],
           StyleBox["6200", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[6200, {}], "Tooltip"]& ]}, 
        {RGBColor[0.928, 0.5210666666666667, 0.2], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{6.0367807720320465`, 0.}, {6.93918426802622, 
               1400.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{234.88890308010446`, 269.80998210747873`}, {
              62.361538842390566`, 96.93728426496513}}],
            StatusArea[#, 1400]& ,
            TagBoxNote->"1400"],
           StyleBox["1400", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[1400, {}], "Tooltip"]& ]}, 
        {RGBColor[0.7116405333333333, 0.4816, 0.5483194666666666], EdgeForm[{
         Opacity[0.644], Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{7.0367807720320465`, 0.}, {7.93918426802622, 
               2200.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{273.0326815341681, 307.9537605615424}, {
              42.8896843152051, 96.93728426496513}}],
            StatusArea[#, 2200]& ,
            TagBoxNote->"2200"],
           StyleBox["2200", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[2200, {}], "Tooltip"]& ]}, 
        {RGBColor[0.4992, 0.5552, 0.8309304], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{8.036780772032046, 0.}, {8.939184268026219, 
               8000.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{311.1764599882318, 
              346.0975390156061}, {-98.28126100688952, 96.93728426496513}}],
            StatusArea[#, 8000]& ,
            TagBoxNote->"8000"],
           StyleBox["8000", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[8000, {}], 
           "Tooltip"]& ]}}}, {}, {}}}, {}, {}}, {}, {}, {}, {}, 
   StyleBox[
    StyleBox[{
      {Thickness[Tiny], 
       LineBox[{{0.5800873998543336, 0.}, {9.106366205389659, 0.}}], 
       StyleBox[{}, "GraphicsLabel",
        StripOnInput->False]}, 
      StyleBox[{
        {Thickness[Tiny], 
         LineBox[{{0.7439912600145666, 0.}, 
           Offset[{-1.102182119232618*^-15, -6.}, {0.7439912600145666, 0.}]}],
          LineBox[{{8.93918426802622, 0.}, 
           Offset[{-1.102182119232618*^-15, -6.}, {8.93918426802622, 
            0.}]}], {{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {
           LineBox[{{4.64639475600874, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {4.64639475600874, 0.}]}],
            LineBox[{{5.0367807720320465`, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {5.0367807720320465`, 
              0.}]}]}}}, 
        StyleBox[{}, "GraphicsLabel",
         StripOnInput->False]}, "GraphicsTicks",
       StripOnInput->False]},
     Antialiasing->False], "GraphicsAxes",
    StripOnInput->False]},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{False, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.5800873998543336, 0.},
  CoordinatesToolOptions:>{"DisplayFunction" -> ({
      Identity[
       Part[#, 1]], 
      Identity[
       Part[#, 2]]}& ), "CopiedValueFunction" -> ({
      Identity[
       Part[#, 1]], 
      Identity[
       Part[#, 2]]}& )},
  DisplayFunction->Identity,
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  PlotRange->{{All, All}, {All, All}},
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{None, Automatic}]], "Output",
 CellChangeTimes->{3.7569577822296124`*^9},
 CellLabel->"Out[78]=",ExpressionUUID->"9c1be917-da75-4c19-b106-e72ad460fb08"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"clist", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"4500", ",", " ", "3000"}], "}"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"2150", ",", " ", "6879"}], "}"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"6200", ",", " ", "1400"}], "}"}], ",", " ", 
    RowBox[{"{", 
     RowBox[{"2200", ",", " ", "8000"}], "}"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.7569577971206336`*^9, 3.756957836693618*^9}},
 CellLabel->"In[79]:=",ExpressionUUID->"0fa0747c-3ad2-4fa3-8fb8-5ec5468e7846"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"4500", ",", "3000"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"2150", ",", "6879"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"6200", ",", "1400"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"2200", ",", "8000"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.756957837947626*^9},
 CellLabel->"Out[79]=",ExpressionUUID->"fca53b69-3560-4fb9-9383-d55845c7cc73"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"BarChart", "[", "clist", "]"}]], "Input",
 CellChangeTimes->{{3.7569578424406433`*^9, 3.756957845949625*^9}},
 CellLabel->"In[80]:=",ExpressionUUID->"de7ea641-77a2-4b44-b51c-4eecb41ea5a6"],

Cell[BoxData[
 GraphicsBox[{
   {Opacity[0], 
    PointBox[{{0.5683758193736344, 0.}, {9.52476329206118, 
     0.}}]}, {{}, {{{}, {{}, {
        {RGBColor[0.982864, 0.7431472, 0.3262672], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{0.7439912600145666, 0.}, {1.64639475600874, 
               4500.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{33.001912529717764`, 
              65.6274915954125}, {-13.091897450453146`, 96.93728426496504}}],
            StatusArea[#, 4500]& ,
            TagBoxNote->"4500"],
           StyleBox["4500", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[4500, {}], "Tooltip"]& ]}, 
        {RGBColor[0.4992, 0.5552, 0.8309304], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{1.7439912600145666`, 0.}, {2.64639475600874, 
               3000.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{68.60192871793315, 101.22750778362787`}, {
              23.41782978801959, 96.93728426496504}}],
            StatusArea[#, 3000]& ,
            TagBoxNote->"3000"],
           StyleBox["3000", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[3000, {}], "Tooltip"]& ]}}}, {}, {}}, {{}, {{}, {
        {RGBColor[0.982864, 0.7431472, 0.3262672], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{3.0367807720320465`, 0.}, {3.93918426802622, 
               2150.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{114.6252562737105, 147.25083533940523`}, {
              44.106675223154134`, 96.93728426496504}}],
            StatusArea[#, 2150]& ,
            TagBoxNote->"2150"],
           StyleBox["2150", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[2150, {}], "Tooltip"]& ]}, 
        {RGBColor[0.4992, 0.5552, 0.8309304], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{4.0367807720320465`, 0.}, {4.93918426802622, 
               6879.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{150.22527246192587`, 
              182.8508515276206}, {-70.99632485067089, 96.93728426496504}}],
            StatusArea[#, 6879]& ,
            TagBoxNote->"6879"],
           StyleBox["6879", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[6879, {}], "Tooltip"]& ]}}}, {}, {}}, {{}, {{}, {
        {RGBColor[0.982864, 0.7431472, 0.3262672], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{5.3295702840495265`, 0.}, {6.2319737800437, 
               6200.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{196.24860001770324`, 
              228.87417908339796`}, {-54.46958832072225, 96.93728426496504}}],
            StatusArea[#, 6200]& ,
            TagBoxNote->"6200"],
           StyleBox["6200", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[6200, {}], "Tooltip"]& ]}, 
        {RGBColor[0.4992, 0.5552, 0.8309304], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{6.3295702840495265`, 0.}, {7.2319737800437, 
               1400.}, "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{231.8486162059186, 264.4741952716133}, {
              62.361538842390495`, 96.93728426496504}}],
            StatusArea[#, 1400]& ,
            TagBoxNote->"1400"],
           StyleBox["1400", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[1400, {}], "Tooltip"]& ]}}}, {}, {}}, {{}, {{}, {
        {RGBColor[0.982864, 0.7431472, 0.3262672], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{7.622359796067006, 0.}, {8.52476329206118, 2200.},
                "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{277.87194376169595`, 310.4975228273907}, {
              42.88968431520504, 96.93728426496504}}],
            StatusArea[#, 2200]& ,
            TagBoxNote->"2200"],
           StyleBox["2200", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[2200, {}], "Tooltip"]& ]}, 
        {RGBColor[0.4992, 0.5552, 0.8309304], EdgeForm[{Opacity[0.644], 
         Thickness[Small]}], 
         TagBox[
          TooltipBox[
           TagBox[
            DynamicBox[{
              FEPrivate`If[
               CurrentValue["MouseOver"], 
               EdgeForm[{
                 GrayLevel[0.5], 
                 AbsoluteThickness[1.5], 
                 Opacity[0.66]}], {}, {}], 
              
              RectangleBox[{8.622359796067007, 0.}, {9.52476329206118, 8000.},
                "RoundingRadius" -> 0]},
             
             ImageSizeCache->{{313.47195994991137`, 
              346.0975390156061}, {-98.28126100688952, 96.93728426496504}}],
            StatusArea[#, 8000]& ,
            TagBoxNote->"8000"],
           StyleBox["8000", {}, StripOnInput -> False]],
          Annotation[#, 
           Style[8000, {}], 
           "Tooltip"]& ]}}}, {}, {}}}, {}, {}}, {}, {}, {}, {}, 
   StyleBox[
    StyleBox[{
      {Thickness[Tiny], 
       LineBox[{{0.5683758193736344, 0.}, {9.703891041514932, 0.}}], 
       StyleBox[{}, "GraphicsLabel",
        StripOnInput->False]}, 
      StyleBox[{
        {Thickness[Tiny], 
         LineBox[{{0.7439912600145666, 0.}, 
           Offset[{-1.102182119232618*^-15, -6.}, {0.7439912600145666, 0.}]}],
          LineBox[{{9.52476329206118, 0.}, 
           Offset[{-1.102182119232618*^-15, -6.}, {9.52476329206118, 
            0.}]}], {{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {
           LineBox[{{2.64639475600874, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {2.64639475600874, 0.}]}],
            LineBox[{{3.0367807720320465`, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {3.0367807720320465`, 
              0.}]}], 
           LineBox[{{4.93918426802622, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {4.93918426802622, 0.}]}],
            LineBox[{{5.3295702840495265`, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {5.3295702840495265`, 
              0.}]}], LineBox[{{7.2319737800437, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {7.2319737800437, 0.}]}], 
           LineBox[{{7.622359796067006, 0.}, 
             Offset[{-1.102182119232618*^-15, -6.}, {7.622359796067006, 
              0.}]}]}}}, 
        StyleBox[{}, "GraphicsLabel",
         StripOnInput->False]}, "GraphicsTicks",
       StripOnInput->False]},
     Antialiasing->False], "GraphicsAxes",
    StripOnInput->False]},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{False, True},
  AxesLabel->{None, None},
  AxesOrigin->{0.5683758193736344, 0.},
  CoordinatesToolOptions:>{"DisplayFunction" -> ({
      Identity[
       Part[#, 1]], 
      Identity[
       Part[#, 2]]}& ), "CopiedValueFunction" -> ({
      Identity[
       Part[#, 1]], 
      Identity[
       Part[#, 2]]}& )},
  DisplayFunction->Identity,
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  PlotRange->{{All, All}, {All, All}},
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{None, Automatic}]], "Output",
 CellChangeTimes->{3.7569578462836246`*^9},
 CellLabel->"Out[80]=",ExpressionUUID->"6fcd7dd7-7b47-4973-839e-95c4b02a9da3"]
}, Open  ]]
},
WindowSize->{636, 773},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 310, 6, 28, "Input",ExpressionUUID->"9ae5341c-9fc5-4ee2-8971-e89cee6cf00d"],
Cell[893, 30, 228, 4, 32, "Output",ExpressionUUID->"c3668c09-653e-4b03-beda-29e457d9b7ab"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1158, 39, 210, 3, 28, "Input",ExpressionUUID->"38484e62-833c-41d4-aa52-772ce79ff3af"],
Cell[1371, 44, 5208, 143, 230, "Output",ExpressionUUID->"f21c34db-7416-4ee1-8ce4-d663bab0626c"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6616, 192, 472, 11, 28, "Input",ExpressionUUID->"b988eef9-3613-4651-bcce-ed3453345743"],
Cell[7091, 205, 359, 9, 32, "Output",ExpressionUUID->"b3cf0d3d-87ea-4537-b415-8cfaa96f0fb9"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7487, 219, 210, 3, 28, "Input",ExpressionUUID->"fd24ca15-8005-4f8f-a956-af869f14395a"],
Cell[7700, 224, 9228, 241, 230, "Output",ExpressionUUID->"9c1be917-da75-4c19-b106-e72ad460fb08"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16965, 470, 540, 13, 28, "Input",ExpressionUUID->"0fa0747c-3ad2-4fa3-8fb8-5ec5468e7846"],
Cell[17508, 485, 434, 12, 32, "Output",ExpressionUUID->"fca53b69-3560-4fb9-9383-d55845c7cc73"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17979, 502, 212, 3, 28, "Input",ExpressionUUID->"de7ea641-77a2-4b44-b51c-4eecb41ea5a6"],
Cell[18194, 507, 9739, 249, 263, "Output",ExpressionUUID->"6fcd7dd7-7b47-4973-839e-95c4b02a9da3"]
}, Open  ]]
}
]
*)

