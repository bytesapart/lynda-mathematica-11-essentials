(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 11.3' *)

(***************************************************************************)
(*                                                                         *)
(*                                                                         *)
(*  Under the Wolfram FreeCDF terms of use, this file and its content are  *)
(*  bound by the Creative Commons BY-SA Attribution-ShareAlike license.    *)
(*                                                                         *)
(*        For additional information concerning CDF licensing, see:        *)
(*                                                                         *)
(*         www.wolfram.com/cdf/adopting-cdf/licensing-options.html         *)
(*                                                                         *)
(*                                                                         *)
(***************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1088,         20]
NotebookDataLength[      9364,        204]
NotebookOptionsPosition[      9763,        196]
NotebookOutlinePosition[     10213,        216]
CellTagsIndexPosition[     10170,        213]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"x", "^", "3"}], " ", "-", " ", 
    RowBox[{"x", "^", "2"}], " ", "+", " ", "x"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{"x", ",", " ", 
     RowBox[{"-", "6"}], ",", " ", "6"}], "}"}]}], "]"}]], "Input",ExpressionU\
UID->"4b9797e9-483b-4c1e-97a6-37acebbf5eea"],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    TagBox[
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJwtWGc8Fe7jvbhEJCvXKEQklTKSSM9DfI0kMlIhW8lKRlQqZc9KdgllF1Lk
Gs8TEonK3tu193aNX//P5//qfM6bc86L8zkvzkFL58s2tAQCQZKGQPg/tMqN
nN3ZIeHskOn+HUEXzEXlE1PcJGHrzMGBfh4XXPXfexOPNRLeLwG2K9ldsFhv
ce3UHAk3SEwHRNK54AmWodSOARIeYcrK8Wtwxi72p4wLKkk4cElUmsfMGfuI
dmLbABKu/vSQ0eyxE45PEHlRz0rC+W38FZp/HXBSX6CzHhM3PiCVabJqcQtz
/9G/dGTfPhwWMV5B52yLa3T1VetEufDnNjcltWorrHFE1E1EmBNrpTT8cBOx
wAofTdJ+n+DAG09pvt+pN8Ubwbac0kfYcXnX6x07+6t4efXdK7PDbFgbHyp+
EGaIq4RfyJY7suLSmLp4kv5lrEj/qOCoNQsmZ/g/L7itg6Pg6dPKD3djX1+X
lkVNTWyVaDwykcSIV3Fhfl6XKv6dLsBEE8+A7eeTK3M/QCzAkieFCol4v1Zj
wECaAnYaFu+O+EjEPEU0n4ihCri8NNnfPI2IxVvIJxXuKGBTx5ddNNFEXCnH
zdGtqIATGzz8VN2JOGevf97G3zOY97lSR60MER/jnil9TXMGc3HXPW7Oo8PX
bHZ4dJxO491ClN/jWbT4foDYrrn7srg7kbGuP4UWG5zg+tVrI4s/8B6tboun
xXKMark9l2SxLqdL6fdgWvz5u8BX5kOyOJphIyPZnhaHl3geEa+XwcLTe3yv
StDiBwvj8qbCMliRfEq2NpMGp4cIPwhrl8IOBn6xGRkEPFS6dtZ6RBInb5Nl
JJIJODpmi92mUhK3Zsw1ZMcR8P7jMdqPkiUx3LpOnxtMwLFTXHU7ppKYK03a
9YsDAT/1Yu840X4cl632Xag4ScBBOVUx5/8ew/vl3LeL7uygmScVr5ZaJLB5
ZV+dwIst9BhEKRscPozVldZsep5sIeFo4ftEpsNYsoiNkHhnC7WU2X34PSGG
N7OUZXn1ttDXe7i28KMYjn2RmsjJtoWCbfWeVJ8Ww38s7BwZwzfRuqvgeL+O
KFYmzLIuBFHRXFPvs8xIESzuvSszz4uK7jxPuPvWXQSzLQmed75FRTaDZjV5
10Rw76iu55QGFY3Ipd45ICqCvX9/6qPsoiIntxpL0RJh/OmNR16X3wZSeFFQ
Ojx5EAsrbet9f7KOsnp85GtvCOE/7ErH4lzXETYbNnPQFMI+lPsMjlbraPlW
gfohGSHcGbFO5lJbR8b9SacbGYTwy8ElEWvGddRx0nRYy0wQEwMnl2kj1tCV
g8ylcJcApjS2x8HEVURcMO6Dnvw4Kp3kxhW2ip6ykgUjL/FjlQdGOmMPV5H8
1d1GDOL8+I1oM23kjVUUuZlw2a6DD1+59/t2n/AqimV/oLOuxIdrDlQr+WSu
oBmJpZsXWHlx5s2CgZLCZZRSO07hqePGzZZJghfSlhGT250E2ffcmGAaYtr5
ahk1sZQz33/EjY30rDrW3JaRBM3jogBZbkyrwPn3lMwyekxOtHz9dh++xnwX
5+YuoZmE0XOpj7nw7o/SSakZiwh7y9Zb3uDApzIEeqRjF5H2yHRA6jkObJ6y
m78iYBFZUxXLmAU4cGH0YHS/7SJqPehy4nIPO7Z89CL8gOgi0j0gefWzKTsm
6y48jEleQHjj43qhDRu2X8w3CYmfR7wWt423RfbgOLl5RbvgeTQT9ZVue4oF
13id5D/vPY9ikY/f+UIWLErI7di4Oo8o3YbqSZosuJ8lx8iedx4tOeuYR91l
xoZiabqasXOoqsSyRrOZCT/5JBvEemwWxTNR7gX+YMC98gXq1cyzqFGzynMo
lgGfRVIMPpMzqDuR75CPPQNeqZN8Op01g4bM9uI7rAzYYUT84a8jM0g1RD5Z
+yo9NuY5cCf48DSS1Xw6/n6DDkv60F9lEJlEx7xq4sLsaTAf+U/MGHES7RvC
/fz/0WD6lYTWn5QJZPzd5Uz3QRrc7ShtEJE1gQasJx4tdBBwsOmNSzzSE8j/
+DThjjYBj54tVpNQHkc8qUNvRd230Vuqg7TOjVH0OGJLINVtA4Welr9zUnkU
qXamsGae3kCed+nyOERGUaHsjBATdR3pTMYdb6NQUPCmqWOr7zra6qwWN3ei
IFbOheHE6DV0jSwk6PpwBGnJOmm+bFhBnF7NzNEJQ0iY9XvJ8sNFtLazzj7r
PYQuaxxauq2+iHr9BXk0rg2hC3+/G0H2RZQRZX9og2cIvdHroKVNW0Dn8ghK
ZtGD6HL8s4aepnl0a/SYk9jzAWSHCCuv4Rzyyd6d7LinD+WP2xybLpxAmsWG
HwR/9aKkvTMr7TcmENePt8V/g3uR0NKLaDumCZQ5KNd4irEXbWyqidqbjaNm
Xiu6bdoetCTRenJm7xiSCCyxCV/rRIf6fjwDkSOozdpBIneoFVkPnFa+SxhA
5T26pwvTWpGTZ1poPu5HaUanVMtutaLujx0Jdk/6kZvGtmndbAtKJN9zlCf2
I7Zjkc9Hqc2o5D2j/ReBXqS5WLAmyNWEWvv0Dx706EQnHWLpD7c1opxnNSEb
xE7EM/KAQzK+EclJ7y/7GNWBKK1qx84KNaI2HjnVlMJ25Etuu2F87C8qk9z9
8jmxDbnTrYYdDGpAHLrDmU4/mpDqMko/sbcB3f+l3WNs1YQ4RgO/Kb2qR76k
CW4mQhP6+JNv+WryLzQeHOtQqNiIKM/Pmbwo/om8aBJ0hUr/IGMhfwm6iWok
TdMwtB/XIU9vifDaV+WIu+z8m0dG35DOzFxrbloZGuLifdlZhpGYZZFgdFEp
GjxZeMdSDKMWTdV8yw4ymg/+rtW7XIZkeM2aqfuLkFaK35SzVTHqdRY5ZaKR
i2oonPd+pOehPzkCfQaXbdHwB76qeQtbRLS4IBR4XgdQNZfpLc/qgEzJFE8W
sisQEPTdq/jOFfR03MpN1gkEeV+q/0unDwSeuRcvTPEkgOh+mu9ytvFANR7E
sfvkgDSpVD20nQWyIuThu5NlYF7lvEGZEhkQq5JKd8mWA2V63Hx7bwm4scZw
5vZpBKy260ur+0pAcN4ED28SBhHrreT4B2XgHG8xe//7CuBwK4bniz4GaROG
dE4F1aCQviIs3KoS3AuLoAQ1NABrmfjvysdrQfitJ63mvm2A3BK8/YW5EQTW
0XwZLm8Dwsw5HzRlG4Hvcd+XN6ltwGw46eKESSPwnPfVdXFrB/Yyj12vfGwE
5l7Pfj6y7QDp/Bl/iZebgFRQQPkbrS5wvqlk/sfrZtCcEZbWzdEHbjaJ8/25
1AYadrP6mV/qA4kXlzo8HrSBGodwq+GQPvBiKUZAP/OfT6w6mwdtPxjwGtPI
p2sH+iYa9Q/v/+NnZFIPl7SDs8Oa6mGOA2DiYrQ3j1wn2LOkrZijNwSCyDX7
Jq17AFuntJWn6xCwLjm/oZTUA7gwT4jKyyFQYVXjv9PRA/hChzvbm4fAUizb
nhbdXiB+6IE3vfEwmOcOpl093wdUDLKLzcxGgFHRfzz4bT9QU3g+cOTRCBC2
aUhH3/qBhpAn03LSCIiteONON9QPLk2pXA0ZGAH3n2YxXBcbACbPOteKrClg
occ61jxvAHh8ZpJnvz0KrDf5tLwbB4FX/OyN7pBRwBctkSCwNggePG4JSM8Z
BZYl1VxcAkPgqXZym9LMKHiXdKp02X4IRA7Le9rfGQM4Ytg9i3EYZHPdLKz0
HAf79ylZhxuMgH63almPZ5Pg8ERgFkVmDHTX0PNlvJsE5Y+GGo2tx0D7frWd
jqpJ4GtYt0/61Rj4U1VZq0Q/BTTELdb3r/3T5fpmRu8/BZLOeMePV4yDpM8l
gVEB0+CmL+N1JsdJEM9IdaxOnwZfRH40afzTjTZR0F/7MQ02kkKTqF2TIJxY
fMCEcQaQIrXOlWtPAR+Dwk8iQTPA64Ad9ZfMNDBbyuv+FDwL6qxqg0t4ZsE1
jblvw1mzQKfMryvkyiwwTDyRzl03C0JDK4q6omeBmHCbfBn9HPBed4z0YZ4D
Aj8N6Q7+NwfEr0ixfAyfA6x8+rFj1XOA9mhBR3zyPJgpvljhWTMPbNmUDhtv
LgJRph9u99vmQd8tMu+3g0vAxBgefkSZBzp0+0Qc1JfAzxXpUH/iAvgp+rko
6OUSeC/DY/QKLoDrnh2vJiWXgcmHwfFPXxfAnnJyRITrCvj51pN9JmMROFtm
1z49sQ5o5uYq54sWwceOpAu1putAHth7LFcvgkpWut/XQtdBWo9J1+bwIjgy
aEeymlgHT/hU3jP/y0H3YOI6e9YGkI9iOXMkbgnwT9cfkJDfBGkByebWQcsg
+eJAs+fzHWCe75ibE7cM1nqLJ1837AC+rjPbS5nL4Gu7ZggXAwGGSzYm+P1c
BtWdPgLyqgTo0ULblsa8AgzKq50oFQT4n4j1xfGwFaDKcmWD+ScNpJSLKTi9
WAVVzBUd3QQifDu2EFiYsgqkBJ+Ji4gR4TUO1Lb9aRVIq7AbpFwgwgabK+4R
Taugjkhh1o4hwkKWwLw8rjXw9PWE/2Upeuh/bVxsMXoNsIiNT19xYYCiK9mc
9xLWQXMXXWIaPxPk+a4nnJ21Dky0FfSd1Jjg7qjVk73F60C7r4Pb0ZkJzkmd
11FtXwdc96ft2KqYYKljZyDbvg3wau5V/oLzbmgwwridEb4BNhjshjUbmaFv
s81YxxMqKHk41GRZwQrdU5lXWZ5Tgepli8bri6zwpms+PXxLBRrlbLTPD+2F
OmxbwmnlVOB6+3VcXsBeyKsdZepKpQJD3BBlLc4G8yorGne7b4JjI1dHlVfZ
YO8nwTLFm1sgUDEoMSmHA9bLu0oIe24BVZGLAZ41HLC0vCqG0X8LiF32bXUa
5oAJP2+5tqRuAb7UmtmS/ZzQeLDgsHPfFqgYeHisMZwT/mXXeJFitA1eX9ar
2rnHBatcXOyY1HZAGMMf/lEbbliwUtE8q78DauSzhf/6ccOUB/tUWi13wFKS
/Eb3e274JJC8P/XxDmiRWLh5jcINz70lNp4t3QE/aeOsZm6SYLedtsBXXgKk
Kj6oDHHjgYYx/YGTkQTo3/arSySND4arF+yhiSNAhjPF3/zq+WDNqt8L7mQC
RCU5BhzLfFDB+Giicj4B3t0qGs5W5YcCvB65MX8JcNPsh+XQCD8ci2duPc9B
A3NomHy9Th6A3m/kRBJf0sAz2xzpsxRB+OZ9aJl2NC18XR92/PSsMHQT105p
e00L1WK+NE8wiEDNbOYAy/e0cPPAaVwgIAKX8oJ1732mhdqfn4T66YjAC6WB
Q6mNtJAR1nNK5IrAtcZnTFRWOpiXt5P5yO0Q1CM8NMwMoIPBPf3XUveKQeJ1
x2l6LyI0SmDe2Qk6AvkvGAnJPPnXSx+JA78yj0BpRaBvHkSENetVkp9rj0Bz
fvZichwRdljk3GhnkoBlXZ+fOZOJUPE97Q3XEAnoYbLJ30Elwg1DMtPay6Nw
3DREK8eHHl5X/1aeSj4OG8wz0y8/Y4BGBirs709KQV+RRwrLIQxQfXnKblZF
Cp6iGNTHvGSAtaHK+uqGUjDxNmGxO4UBakd83xT0loK3Pa6cs/vGAF+5nD/u
XiUFGUPpW+5vM0B/7ttRBdeloUqRBe37e7vgsO+e7XdRMvDLHj7TVQdG2OZe
ou8jJQfF4g+O7dLeDenPpERSdxThhsezrWx9FhjyosWi/ZMynNzgZ82QZ4VR
pWKvzL+owbd2Ac38Imxw8CNNVswXLRip/CuUV5QdHiqj7AISupDVOVulch8H
ZKaM3NwvpA/LVb+WFfJxQrmAREZbshHccrwhZ8TKBaPNZVJ071+DNLsa445u
cUEj7zkNtlAzKKfhWqFF4IaHTuplln62gBvdOWP7Frhh9/xddUYZa6jqUBN5
cYUbhpfTj2afs4Zh1CF5vw1u+IX3E6OBljUU4uMPWqIlwfTU3pZ8C2uofiXo
SBMnCXYKHspLjbSGUX9t7CNPkeCpAM0jFrPWULJaYGq3FwmGPFY11MyzgfcM
FaJUHpJgafzRKZ9SG/ht2PCs9xMSvOanc7C4xgYa0IWFjgeR4NGluluKAzbw
PqQeq4knwSY9lVBPDltYQ25z9CslQYYu+u4Gd1vIobW4rwyTYH6BGNbxtYUm
HazlS1Uk+GPrpW5TuC2cXVVjta4nwYes7V4T6baQ+9Tnjyq9JNigmMqg12kL
zat+G3kPkuC74iqbFYotzNSf3M6nkKDVusK9t4u28Kyr8KWDsyQ4Rskk0+6x
g/4051aNF0nwd4b1FJnXDv6JvJoUuUqCgwZrzZ5idpBPyF29hkqC0stq7mdk
7OD//1mw5qXkv/W3g/8DzXx6+g==
       "]]},
     Annotation[#, "Charting`Private`Tag$52204#1"]& ]}, {}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}, {Automatic, 
     Charting`ScaledFrameTicks[{Identity, Identity}]}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{-6, 6}, {-257.9999703673481, 185.99997624489902`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",ExpressionUUID->"62dd4270-4bcc-\
4835-9fd9-5bf706440c59"]
}, Open  ]]
},
WindowSize->{637, 773},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
Deployed->True,
CellContext->Notebook,
TrackCellChangeTimes->False,
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1510, 35, 336, 9, 28, "Input",ExpressionUUID->"4b9797e9-483b-4c1e-97a6-37acebbf5eea"],
Cell[1849, 46, 7898, 147, 241, "Output",ExpressionUUID->"62dd4270-4bcc-4835-9fd9-5bf706440c59"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature ExTzL3SSi@peXDgMAiku2OBa *)
