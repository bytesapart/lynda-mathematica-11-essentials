(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      7234,        223]
NotebookOptionsPosition[      4973,        174]
NotebookOutlinePosition[      5314,        189]
CellTagsIndexPosition[      5271,        186]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"list8", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{
   "7", ",", " ", "24", ",", " ", "81", ",", " ", "95", ",", " ", "81", ",", 
    " ", "5"}], "}"}]}]], "Input",
 CellChangeTimes->{{3.7568944189400854`*^9, 3.7568944297240877`*^9}},
 CellLabel->
  "In[105]:=",ExpressionUUID->"e646f9b5-a5b2-4fdd-8878-3929064e15d2"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"7", ",", "24", ",", "81", ",", "95", ",", "81", ",", "5"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.7568944310700817`*^9},
 CellLabel->
  "Out[105]=",ExpressionUUID->"a1b37c4d-b5bd-4554-b7a6-c8efcc75dd2d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Mean", "[", "list8", "]"}]], "Input",
 CellChangeTimes->{{3.7568944330920887`*^9, 3.756894438278082*^9}},
 CellLabel->
  "In[106]:=",ExpressionUUID->"f053af36-eafe-4883-8edd-a469dbe44e43"],

Cell[BoxData[
 FractionBox["293", "6"]], "Output",
 CellChangeTimes->{3.756894438543083*^9},
 CellLabel->
  "Out[106]=",ExpressionUUID->"116759fe-cccd-4a8d-8b68-901f8feedc09"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"N", "[", 
  FractionBox["293", "6"], "]"}]], "Input",
 NumberMarks->False,
 CellLabel->
  "In[107]:=",ExpressionUUID->"5408e292-6080-4a76-8b46-e7e411e52bb6"],

Cell[BoxData["48.833333333333336`"], "Output",
 CellChangeTimes->{3.7568944469290905`*^9},
 CellLabel->
  "Out[107]=",ExpressionUUID->"f89aa310-912d-4831-b2a7-b6e91e91094d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Median", "[", "list8", "]"}]], "Input",
 CellChangeTimes->{{3.756894448748084*^9, 3.7568944516420937`*^9}},
 CellLabel->
  "In[108]:=",ExpressionUUID->"e7f4c375-6e72-49bd-890d-34b5096e6ef7"],

Cell[BoxData[
 FractionBox["105", "2"]], "Output",
 CellChangeTimes->{3.756894451877082*^9},
 CellLabel->
  "Out[108]=",ExpressionUUID->"c18efc63-76cb-4a07-8354-49a38ba74b77"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"N", "[", 
  FractionBox["105", "2"], "]"}]], "Input",
 NumberMarks->False,
 CellLabel->
  "In[109]:=",ExpressionUUID->"67901f49-3ca5-4d19-b300-9474166a2837"],

Cell[BoxData["52.5`"], "Output",
 CellChangeTimes->{3.7568944548710823`*^9},
 CellLabel->
  "Out[109]=",ExpressionUUID->"10d2ee16-475e-4754-93fe-10d2fc76e87a"]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.7568944405160804`*^9, 
  3.7568944603760853`*^9}},ExpressionUUID->"387fa21f-49b0-44ec-9965-\
8664c47cec63"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Commonest", "[", "list8", "]"}]], "Input",
 CellChangeTimes->{{3.7568944838050833`*^9, 3.7568944946110835`*^9}},
 CellLabel->
  "In[110]:=",ExpressionUUID->"031bd77b-25ba-4e28-9713-ce8a7c0f1a20"],

Cell[BoxData[
 RowBox[{"{", "81", "}"}]], "Output",
 CellChangeTimes->{3.7568944953340845`*^9},
 CellLabel->
  "Out[110]=",ExpressionUUID->"981d3471-fe92-45a9-a07b-776ba79eaae8"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"list8", " ", "=", " ", 
  RowBox[{"Append", "[", 
   RowBox[{"list8", ",", " ", "24"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7568945017990828`*^9, 3.7568945088690977`*^9}},
 CellLabel->
  "In[111]:=",ExpressionUUID->"cd740273-e88e-4adb-9ead-af53c1186a86"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "7", ",", "24", ",", "81", ",", "95", ",", "81", ",", "5", ",", "24"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.7568945094481134`*^9},
 CellLabel->
  "Out[111]=",ExpressionUUID->"6a57f4bb-dd32-4373-939e-e0ae188e01d3"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Commonest", "[", 
  RowBox[{"list8", ",", " ", "2"}], "]"}]], "Input",
 CellChangeTimes->{{3.7568945123220854`*^9, 3.7568945226140966`*^9}},
 CellLabel->
  "In[112]:=",ExpressionUUID->"289d4ee4-1a6e-4b5d-b49f-152876eae792"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"24", ",", "81"}], "}"}]], "Output",
 CellChangeTimes->{3.7568945230550823`*^9},
 CellLabel->
  "Out[112]=",ExpressionUUID->"04d535f4-b81e-4f2a-b480-216d24812448"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Commonest", "[", 
  RowBox[{"list8", ",", " ", "1"}], "]"}]], "Input",
 CellChangeTimes->{{3.7568945308011117`*^9, 3.756894543907143*^9}},
 CellLabel->
  "In[114]:=",ExpressionUUID->"2c0fb2f7-d06a-471d-a7ec-efc3af8bc0ab"],

Cell[BoxData[
 RowBox[{"{", "24", "}"}]], "Output",
 CellChangeTimes->{{3.7568945362421303`*^9, 3.756894544485131*^9}},
 CellLabel->
  "Out[114]=",ExpressionUUID->"7e498c07-e1e6-4c35-8ead-72c23bf972ba"]
}, Open  ]]
},
WindowSize->{636, 988},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 341, 8, 28, "Input",ExpressionUUID->"e646f9b5-a5b2-4fdd-8878-3929064e15d2"],
Cell[924, 32, 247, 6, 32, "Output",ExpressionUUID->"a1b37c4d-b5bd-4554-b7a6-c8efcc75dd2d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1208, 43, 212, 4, 28, "Input",ExpressionUUID->"f053af36-eafe-4883-8edd-a469dbe44e43"],
Cell[1423, 49, 175, 4, 55, "Output",ExpressionUUID->"116759fe-cccd-4a8d-8b68-901f8feedc09"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1635, 58, 181, 5, 51, "Input",ExpressionUUID->"5408e292-6080-4a76-8b46-e7e411e52bb6"],
Cell[1819, 65, 173, 3, 32, "Output",ExpressionUUID->"f89aa310-912d-4831-b2a7-b6e91e91094d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2029, 73, 214, 4, 28, "Input",ExpressionUUID->"e7f4c375-6e72-49bd-890d-34b5096e6ef7"],
Cell[2246, 79, 175, 4, 55, "Output",ExpressionUUID->"c18efc63-76cb-4a07-8354-49a38ba74b77"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2458, 88, 181, 5, 51, "Input",ExpressionUUID->"67901f49-3ca5-4d19-b300-9474166a2837"],
Cell[2642, 95, 159, 3, 32, "Output",ExpressionUUID->"10d2ee16-475e-4754-93fe-10d2fc76e87a"]
}, Open  ]],
Cell[2816, 101, 156, 3, 28, "Input",ExpressionUUID->"387fa21f-49b0-44ec-9965-8664c47cec63"],
Cell[CellGroupData[{
Cell[2997, 108, 219, 4, 28, "Input",ExpressionUUID->"031bd77b-25ba-4e28-9713-ce8a7c0f1a20"],
Cell[3219, 114, 178, 4, 32, "Output",ExpressionUUID->"981d3471-fe92-45a9-a07b-776ba79eaae8"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3434, 123, 283, 6, 28, "Input",ExpressionUUID->"cd740273-e88e-4adb-9ead-af53c1186a86"],
Cell[3720, 131, 261, 7, 32, "Output",ExpressionUUID->"6a57f4bb-dd32-4373-939e-e0ae188e01d3"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4018, 143, 247, 5, 28, "Input",ExpressionUUID->"289d4ee4-1a6e-4b5d-b49f-152876eae792"],
Cell[4268, 150, 202, 5, 32, "Output",ExpressionUUID->"04d535f4-b81e-4f2a-b480-216d24812448"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4507, 160, 245, 5, 28, "Input",ExpressionUUID->"2c0fb2f7-d06a-471d-a7ec-efc3af8bc0ab"],
Cell[4755, 167, 202, 4, 65, "Output",ExpressionUUID->"7e498c07-e1e6-4c35-8ead-72c23bf972ba"]
}, Open  ]]
}
]
*)

