(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      8915,        269]
NotebookOptionsPosition[      7178,        229]
NotebookOutlinePosition[      7519,        244]
CellTagsIndexPosition[      7476,        241]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"mat3", " ", "=", " ", 
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"4", "8", "12"},
     {"9", "10", "11"}
    }], "\[NoBreak]", ")"}]}]], "Input",
 CellChangeTimes->{{3.756956554383311*^9, 3.7569565868243103`*^9}},
 CellLabel->"In[26]:=",ExpressionUUID->"6a3a36c0-d47b-49f1-88bc-b685547285f9"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"4", ",", "8", ",", "12"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"9", ",", "10", ",", "11"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.7569565885023003`*^9},
 CellLabel->"Out[26]=",ExpressionUUID->"12e877f2-7246-446d-bedc-c2f0f74a2a60"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"mat4", " ", "=", " ", 
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"1", "2", "3"},
     {"4", "5", "6"}
    }], "\[NoBreak]", ")"}]}]], "Input",
 CellChangeTimes->{{3.7569565898543034`*^9, 3.756956599851321*^9}},
 CellLabel->"In[27]:=",ExpressionUUID->"4da26908-7661-4be0-80a4-9a8599443841"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "2", ",", "3"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"4", ",", "5", ",", "6"}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.756956602052305*^9},
 CellLabel->"Out[27]=",ExpressionUUID->"57036990-2022-4686-a5d4-5419f009ae5d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"mat3", " ", "+", " ", "mat4"}], " ", "//", " ", 
  "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.756956604739304*^9, 3.756956609593337*^9}},
 CellLabel->"In[28]:=",ExpressionUUID->"af14d99c-8cd4-4602-ac34-ef6f3a014d0f"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"5", "10", "15"},
     {"13", "15", "17"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.756956610049324*^9},
 CellLabel->
  "Out[28]//MatrixForm=",ExpressionUUID->"3da4702d-e9ee-4b38-bb40-\
6e8c94e72b8d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"mat3", " ", "-", " ", "mat4"}], " ", "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.756956616771346*^9, 3.75695662189233*^9}},
 CellLabel->"In[29]:=",ExpressionUUID->"cb1146ba-118f-4ecd-af0f-4ec57fee85c6"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"3", "6", "9"},
     {"5", "5", "5"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.756956622302372*^9},
 CellLabel->
  "Out[29]//MatrixForm=",ExpressionUUID->"d5dd6f47-b2ba-420b-a446-\
d44ee1dbcb5a"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"mat3", " ", "*", " ", "mat4"}], " ", "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.756956628662342*^9, 3.7569566397243395`*^9}},
 CellLabel->"In[31]:=",ExpressionUUID->"afef58ed-e8b8-4818-ac5e-04c0aa1a2df2"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"4", "16", "36"},
     {"36", "50", "66"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{{3.7569566344723244`*^9, 3.7569566401363287`*^9}},
 CellLabel->
  "Out[31]//MatrixForm=",ExpressionUUID->"c2fde85d-e528-46f5-a34b-\
0ce8aa3ecbcd"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"mat3", " ", "/", " ", "mat4"}], " ", "//", "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.75695664261633*^9, 3.756956647698333*^9}},
 CellLabel->"In[32]:=",ExpressionUUID->"9458cd8a-3464-4f5a-8dff-4d773bc4998e"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"4", "4", "4"},
     {
      FractionBox["9", "4"], "2", 
      FractionBox["11", "6"]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.7569566481113386`*^9},
 CellLabel->
  "Out[32]//MatrixForm=",ExpressionUUID->"054f41b3-4f63-4643-b15a-\
11d8beace8c7"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"N", "[", 
   RowBox[{"mat3", " ", "/", " ", "mat4"}], "]"}], " ", "//", " ", 
  "MatrixForm"}]], "Input",
 CellChangeTimes->{{3.756956650195342*^9, 3.7569566563503304`*^9}},
 CellLabel->"In[33]:=",ExpressionUUID->"dfc8523f-ddb0-4eba-ab7a-70b95b69f40c"],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {"4.`", "4.`", "4.`"},
     {"2.25`", "2.`", "1.8333333333333333`"}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.7569566567663317`*^9},
 CellLabel->
  "Out[33]//MatrixForm=",ExpressionUUID->"8ca40f5b-c931-4cb0-9983-\
d8a2ce878a32"]
}, Open  ]]
},
WindowSize->{636, 773},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 319, 7, 44, "Input",ExpressionUUID->"6a3a36c0-d47b-49f1-88bc-b685547285f9"],
Cell[902, 31, 317, 8, 32, "Output",ExpressionUUID->"12e877f2-7246-446d-bedc-c2f0f74a2a60"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1256, 44, 316, 7, 44, "Input",ExpressionUUID->"4da26908-7661-4be0-80a4-9a8599443841"],
Cell[1575, 53, 312, 8, 32, "Output",ExpressionUUID->"57036990-2022-4686-a5d4-5419f009ae5d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1924, 66, 256, 5, 28, "Input",ExpressionUUID->"af14d99c-8cd4-4602-ac34-ef6f3a014d0f"],
Cell[2183, 73, 738, 21, 62, "Output",ExpressionUUID->"3da4702d-e9ee-4b38-bb40-6e8c94e72b8d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2958, 99, 247, 4, 28, "Input",ExpressionUUID->"cb1146ba-118f-4ecd-af0f-4ec57fee85c6"],
Cell[3208, 105, 733, 21, 62, "Output",ExpressionUUID->"d5dd6f47-b2ba-420b-a446-d44ee1dbcb5a"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3978, 131, 250, 4, 28, "Input",ExpressionUUID->"afef58ed-e8b8-4818-ac5e-04c0aa1a2df2"],
Cell[4231, 137, 766, 21, 62, "Output",ExpressionUUID->"c2fde85d-e528-46f5-a34b-0ce8aa3ecbcd"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5034, 163, 247, 4, 28, "Input",ExpressionUUID->"9458cd8a-3464-4f5a-8dff-4d773bc4998e"],
Cell[5284, 169, 786, 23, 74, "Output",ExpressionUUID->"054f41b3-4f63-4643-b15a-11d8beace8c7"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6107, 197, 287, 6, 28, "Input",ExpressionUUID->"dfc8523f-ddb0-4eba-ab7a-70b95b69f40c"],
Cell[6397, 205, 765, 21, 119, "Output",ExpressionUUID->"8ca40f5b-c931-4cb0-9983-d8a2ce878a32"]
}, Open  ]]
}
]
*)

