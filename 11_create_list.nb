(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      3944,        117]
NotebookOptionsPosition[      2874,         89]
NotebookOutlinePosition[      3215,        104]
CellTagsIndexPosition[      3172,        101]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"alist", " ", "=", " ", 
  RowBox[{"{", 
   RowBox[{"1", ",", "1", ",", "2", ",", "3", ",", "5", ",", "8"}], 
   "}"}]}]], "Input",
 CellChangeTimes->{{3.7568849293245173`*^9, 3.756884938312503*^9}},
 CellLabel->"In[43]:=",ExpressionUUID->"d06cafa3-bf93-4bf3-9b09-d25be6a68222"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"1", ",", "1", ",", "2", ",", "3", ",", "5", ",", "8"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.7568849400895033`*^9},
 CellLabel->"Out[43]=",ExpressionUUID->"46525c65-42ea-45b1-ab22-4f418b6aed5f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"blist", " ", "=", " ", 
  RowBox[{"Range", "[", "5", "]"}]}]], "Input",
 CellChangeTimes->{{3.756884943049513*^9, 3.7568849674185605`*^9}},
 CellLabel->"In[45]:=",ExpressionUUID->"eed3cc90-c994-4c50-a207-fe930319ba0a"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"1", ",", "2", ",", "3", ",", "4", ",", "5"}], "}"}]], "Output",
 CellChangeTimes->{3.7568849677995005`*^9},
 CellLabel->"Out[45]=",ExpressionUUID->"bfb610d5-7c56-4806-bb7c-90609480eca4"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"clist", " ", "=", " ", 
  RowBox[{"Range", "[", 
   RowBox[{"2", ",", "8"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7568849725545044`*^9, 3.7568849772605047`*^9}},
 CellLabel->"In[46]:=",ExpressionUUID->"c613049a-31d5-4906-9bc6-9c2e7d45063d"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"2", ",", "3", ",", "4", ",", "5", ",", "6", ",", "7", ",", "8"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.756884978562502*^9},
 CellLabel->"Out[46]=",ExpressionUUID->"ee4d422f-d953-45e0-8905-fd47ecdbdb07"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"dlist", " ", "=", " ", 
  RowBox[{"Range", "[", 
   RowBox[{"2", ",", "8", ",", " ", "0.5"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7568849890665183`*^9, 3.756884996413528*^9}},
 CellLabel->"In[47]:=",ExpressionUUID->"a50ce91a-eaa4-4877-8571-7e358b36d202"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "2.`", ",", "2.5`", ",", "3.`", ",", "3.5`", ",", "4.`", ",", "4.5`", ",", 
   "5.`", ",", "5.5`", ",", "6.`", ",", "6.5`", ",", "7.`", ",", "7.5`", ",", 
   "8.`"}], "}"}]], "Output",
 CellChangeTimes->{3.7568849967405014`*^9},
 CellLabel->"Out[47]=",ExpressionUUID->"32fcccc4-e519-4a98-b49a-9d70b80c9ead"]
}, Open  ]]
},
WindowSize->{958, 988},
WindowMargins->{{Automatic, -7}, {Automatic, 0}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 301, 6, 28, "Input",ExpressionUUID->"d06cafa3-bf93-4bf3-9b09-d25be6a68222"],
Cell[884, 30, 239, 5, 32, "Output",ExpressionUUID->"46525c65-42ea-45b1-ab22-4f418b6aed5f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1160, 40, 242, 4, 28, "Input",ExpressionUUID->"eed3cc90-c994-4c50-a207-fe930319ba0a"],
Cell[1405, 46, 226, 4, 32, "Output",ExpressionUUID->"bfb610d5-7c56-4806-bb7c-90609480eca4"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1668, 55, 268, 5, 28, "Input",ExpressionUUID->"c613049a-31d5-4906-9bc6-9c2e7d45063d"],
Cell[1939, 62, 247, 5, 32, "Output",ExpressionUUID->"ee4d422f-d953-45e0-8905-fd47ecdbdb07"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2223, 72, 283, 5, 28, "Input",ExpressionUUID->"a50ce91a-eaa4-4877-8571-7e358b36d202"],
Cell[2509, 79, 349, 7, 65, "Output",ExpressionUUID->"32fcccc4-e519-4a98-b49a-9d70b80c9ead"]
}, Open  ]]
}
]
*)

