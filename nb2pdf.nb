(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     20366,        477]
NotebookOptionsPosition[     19558,        457]
NotebookOutlinePosition[     19900,        472]
CellTagsIndexPosition[     19857,        469]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.756961549002101*^9, 3.756961549018108*^9}, 
   3.756962164147631*^9},ExpressionUUID->"1b8bac8f-a982-4175-b5fd-\
e1348f7fb17a"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"notebookfiles", "=", 
  RowBox[{"FileNames", "[", "\"\<A\\\\*.nb\>\"", "]"}]}], "\n", 
 RowBox[{"openednotebooks", "=", 
  RowBox[{
   RowBox[{
    RowBox[{"NotebookOpen", "[", 
     RowBox[{
      RowBox[{"StringJoin", "[", 
       RowBox[{
        RowBox[{"Directory", "[", "]"}], ",", "\"\<\\\\\>\"", ",", "#"}], 
       "]"}], ",", 
      RowBox[{"Visible", "\[Rule]", "False"}]}], "]"}], "&"}], "/@", 
   "notebookfiles"}]}], "\n", 
 RowBox[{"MapThread", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"NotebookPrint", "[", 
     RowBox[{"#1", ",", 
      RowBox[{"StringJoin", "[", 
       RowBox[{"#2", ",", "\"\<.pdf\>\""}], "]"}]}], "]"}], "&"}], ",", 
   RowBox[{"{", 
    RowBox[{"openednotebooks", ",", "notebookfiles"}], "}"}]}], "]"}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"NotebookClose", "[", "#", "]"}], "&"}], "/@", 
  "openednotebooks"}]}], "Input",
 CellChangeTimes->{{3.756961795079093*^9, 3.756961825263111*^9}, {
  3.7569618560041056`*^9, 3.7569618592401133`*^9}, {3.7569619445896378`*^9, 
  3.756961946661622*^9}},
 CellLabel->"In[27]:=",ExpressionUUID->"a734705a-6748-4ad2-8d00-4cb7b9851ace"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"\<\"A\\\\01_run_mathematica.nb\"\>", 
   ",", "\<\"A\\\\02_manage_mathematica_notebooks.nb\"\>", 
   ",", "\<\"A\\\\03_help_and_documentation.nb\"\>", 
   ",", "\<\"A\\\\04_intro_operators.nb\"\>", 
   ",", "\<\"A\\\\05_refer_previous.nb\"\>", 
   ",", "\<\"A\\\\06_assign_values.nb\"\>", 
   ",", "\<\"A\\\\07_import_export.nb\"\>", 
   ",", "\<\"A\\\\08_inbuilt_functions_and_variables.nb\"\>", 
   ",", "\<\"A\\\\09_using_math_assistant.nb\"\>", 
   ",", "\<\"A\\\\10_wolfram_alpha_call.nb\"\>", 
   ",", "\<\"A\\\\11_create_list.nb\"\>", 
   ",", "\<\"A\\\\11_refer_and_edit_list.nb\"\>", 
   ",", "\<\"A\\\\12_add_and_delete_items.nb\"\>", 
   ",", "\<\"A\\\\13_count_and_summarize.nb\"\>", 
   ",", "\<\"A\\\\14_sort_and_reorder_lists.nb\"\>", 
   ",", "\<\"A\\\\15_first_last_item_in_list.nb\"\>", 
   ",", "\<\"A\\\\16_select_list.nb\"\>", 
   ",", "\<\"A\\\\17_delete_duplicate_list_items.nb\"\>", 
   ",", "\<\"A\\\\18_join_lists.nb\"\>", 
   ",", "\<\"A\\\\19_mean_median_commenest.nb\"\>", 
   ",", "\<\"A\\\\20_variance_std.nb\"\>", 
   ",", "\<\"A\\\\21_quartiles.nb\"\>", 
   ",", "\<\"A\\\\22_quartile_range.nb\"\>", 
   ",", "\<\"A\\\\23_calculate_covariance.nb\"\>", 
   ",", "\<\"A\\\\24_correlation.nb\"\>", 
   ",", "\<\"A\\\\25_matrices_and_vectors.nb\"\>", 
   ",", "\<\"A\\\\26_add_sub_mul_mat.nb\"\>", 
   ",", "\<\"A\\\\27_generate_useful_matrices.nb\"\>", 
   ",", "\<\"A\\\\28_transpose_invert_mat.nb\"\>", 
   ",", "\<\"A\\\\29_element_wise_calc.nb\"\>", 
   ",", "\<\"A\\\\30_refer_and_edit_matrix.nb\"\>", 
   ",", "\<\"A\\\\31_mat_to_list.nb\"\>", 
   ",", "\<\"A\\\\32_suppress_output.nb\"\>", 
   ",", "\<\"A\\\\33_defining_a_function.nb\"\>", 
   ",", "\<\"A\\\\34_comments.nb\"\>", ",", "\<\"A\\\\35_mat_stuff.nb\"\>", 
   ",", "\<\"A\\\\36_creating_bar_charts.nb\"\>", 
   ",", "\<\"A\\\\37_summarize_data.nb\"\>", 
   ",", "\<\"A\\\\38_scatter_plot.nb\"\>", 
   ",", "\<\"A\\\\39_paired_barchart.nb\"\>", 
   ",", "\<\"A\\\\40_set_chart_options.nb\"\>", 
   ",", "\<\"A\\\\41_plot_function_output.nb\"\>", 
   ",", "\<\"A\\\\42_create_cdf.nb\"\>", 
   ",", "\<\"A\\\\43_maipulate_single_input_value.nb\"\>", 
   ",", "\<\"A\\\\44_manipulate_two_or_more_input_values.nb\"\>", 
   ",", "\<\"A\\\\45_animate_results_of_expression.nb\"\>"}], "}"}]], "Output",
 CellChangeTimes->{3.7569618643680925`*^9, 3.756961949396615*^9},
 CellLabel->"Out[27]=",ExpressionUUID->"d98f8a5b-a8f1-494c-9458-9a82630f9ee3"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],169,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","169",
     "\"01_run_mathematica.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\01_run_mathematica.nb\
\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],170,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","170",
     "\"02_manage_mathematica_notebooks.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\02_manage_mathematica_notebooks.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],171,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","171",
     "\"03_help_and_documentation.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\03_help_and_documentation.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],172,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","172",
     "\"04_intro_operators.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\04_intro_operators.nb\
\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],173,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","173",
     "\"05_refer_previous.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\05_refer_previous.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],174,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","174",
     "\"06_assign_values.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\06_assign_values.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],175,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","175",
     "\"07_import_export.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\07_import_export.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],176,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","176",
     "\"08_inbuilt_functions_and_variables.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\08_inbuilt_functions_and_variables.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],177,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","177",
     "\"09_using_math_assistant.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\09_using_math_assistant.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],178,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","178",
     "\"10_wolfram_alpha_call.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\10_wolfram_alpha_call.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],179,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","179",
     "\"11_create_list.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\11_create_list.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],180,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","180",
     "\"11_refer_and_edit_list.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\11_refer_and_edit_list.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],181,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","181",
     "\"12_add_and_delete_items.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\12_add_and_delete_items.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],182,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","182",
     "\"13_count_and_summarize.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\13_count_and_summarize.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],183,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","183",
     "\"14_sort_and_reorder_lists.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\14_sort_and_reorder_lists.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],184,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","184",
     "\"15_first_last_item_in_list.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\15_first_last_item_in_list.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],185,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","185",
     "\"16_select_list.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\16_select_list.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],186,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","186",
     "\"17_delete_duplicate_list_items.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\17_delete_duplicate_list_items.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],187,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","187",
     "\"18_join_lists.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\18_join_lists.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],188,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","188",
     "\"19_mean_median_commenest.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\19_mean_median_commenest.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],189,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","189",
     "\"20_variance_std.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\20_variance_std.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],190,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","190",
     "\"21_quartiles.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\21_quartiles.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],191,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","191",
     "\"22_quartile_range.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\22_quartile_range.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],192,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","192",
     "\"23_calculate_covariance.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\23_calculate_covariance.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],193,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","193",
     "\"24_correlation.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\24_correlation.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],194,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","194",
     "\"25_matrices_and_vectors.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\25_matrices_and_vectors.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],195,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","195",
     "\"26_add_sub_mul_mat.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\26_add_sub_mul_mat.nb\
\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],196,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","196",
     "\"27_generate_useful_matrices.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\27_generate_useful_matrices.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],197,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","197",
     "\"28_transpose_invert_mat.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\28_transpose_invert_mat.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],198,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","198",
     "\"29_element_wise_calc.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\29_element_wise_calc.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],199,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","199",
     "\"30_refer_and_edit_matrix.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\30_refer_and_edit_matrix.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],200,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","200",
     "\"31_mat_to_list.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\31_mat_to_list.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],201,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","201",
     "\"32_suppress_output.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\32_suppress_output.nb\
\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],202,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","202",
     "\"33_defining_a_function.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\33_defining_a_function.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],203,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","203",
     "\"34_comments.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\34_comments.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],204,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","204",
     "\"35_mat_stuff.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\35_mat_stuff.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],205,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","205",
     "\"36_creating_bar_charts.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\36_creating_bar_charts.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],206,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","206",
     "\"37_summarize_data.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\37_summarize_data.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],207,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","207",
     "\"38_scatter_plot.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\38_scatter_plot.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],208,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","208",
     "\"39_paired_barchart.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\39_paired_barchart.nb\
\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],209,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","209",
     "\"40_set_chart_options.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\40_set_chart_options.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],210,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","210",
     "\"41_plot_function_output.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\41_plot_function_output.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],211,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","211",
     "\"42_create_cdf.nb\"",
     "\"C:\\\\Users\\\\Osama Iqbal\\\\Documents\\\\A\\\\42_create_cdf.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],212,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","212",
     "\"43_maipulate_single_input_value.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\43_maipulate_single_input_value.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],213,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","213",
     "\"44_manipulate_two_or_more_input_values.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\44_manipulate_two_or_more_input_values.nb\""},
    "NotebookObject"], ",", 
   TemplateBox[{FrontEndObject[
      LinkObject["risst_shm", 3, 1]],214,
     "FrontEndObject[LinkObject[\"risst_shm\", 3, 1]]","214",
     "\"45_animate_results_of_expression.nb\"",
     "\"C:\\\\Users\\\\Osama \
Iqbal\\\\Documents\\\\A\\\\45_animate_results_of_expression.nb\""},
    "NotebookObject"]}], "}"}]], "Output",
 CellChangeTimes->{3.7569618643680925`*^9, 3.756961957080617*^9},
 CellLabel->"Out[28]=",ExpressionUUID->"209041c9-814c-4baa-a182-229d4c24312b"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", 
   ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", 
   "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", 
   ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", 
   "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", 
   ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", 
   "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", 
   ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", 
   "Null", ",", "Null"}], "}"}]], "Output",
 CellChangeTimes->{3.7569618643680925`*^9, 3.7569619797566156`*^9},
 CellLabel->"Out[29]=",ExpressionUUID->"ede00780-0325-421b-b7f4-7b31269841cb"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", 
   ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", 
   "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", 
   ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", 
   "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", 
   ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", 
   "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", 
   ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", "Null", ",", 
   "Null", ",", "Null"}], "}"}]], "Output",
 CellChangeTimes->{3.7569618643680925`*^9, 3.756961981724621*^9},
 CellLabel->"Out[30]=",ExpressionUUID->"98968321-5e8d-4083-919c-28579cb3d6e9"]
}, Open  ]]
},
WindowSize->{759, 813},
WindowMargins->{{Automatic, 522}, {2, Automatic}},
FrontEndVersion->"11.3 for Microsoft Windows (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 175, 3, 28, "Input",ExpressionUUID->"1b8bac8f-a982-4175-b5fd-e1348f7fb17a"],
Cell[CellGroupData[{
Cell[758, 27, 1138, 30, 105, "Input",ExpressionUUID->"a734705a-6748-4ad2-8d00-4cb7b9851ace"],
Cell[1899, 59, 2478, 48, 318, "Output",ExpressionUUID->"d98f8a5b-a8f1-494c-9458-9a82630f9ee3"],
Cell[4380, 109, 13492, 315, 1011, "Output",ExpressionUUID->"209041c9-814c-4baa-a182-229d4c24312b"],
Cell[17875, 426, 833, 13, 71, "Output",ExpressionUUID->"ede00780-0325-421b-b7f4-7b31269841cb"],
Cell[18711, 441, 831, 13, 104, "Output",ExpressionUUID->"98968321-5e8d-4083-919c-28579cb3d6e9"]
}, Open  ]]
}
]
*)

